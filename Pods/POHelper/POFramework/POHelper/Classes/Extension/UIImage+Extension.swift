/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 4/12/17.
 */

import UIKit

public extension UIImage {
  convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
    let rect = CGRect(origin: .zero, size: size)
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
    color.setFill()
    UIRectFill(rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    guard let cgImage = image?.cgImage else { return nil }
    self.init(cgImage: cgImage)
  }

  func transform(withNewColor color: UIColor) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(size, false, scale)

    let context = UIGraphicsGetCurrentContext()!
    context.translateBy(x: 0, y: size.height)
    context.scaleBy(x: 1.0, y: -1.0)
    context.setBlendMode(.normal)

    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    context.clip(to: rect, mask: cgImage!)

    color.setFill()
    context.fill(rect)

    let newImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return newImage
  }

  convenience init(view: UIView) {
    UIGraphicsBeginImageContextWithOptions(view.frame.size, view.isOpaque, 0.0)
    view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)

    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    self.init(cgImage: (image?.cgImage)!)
  }

  func cropWithBounds(bounds: CGRect) -> UIImage {
    let imageRef = cgImage?.cropping(to: bounds)
    return UIImage(cgImage: imageRef!)
  }

  func updateImageOrientionUpSide() -> UIImage? {
    if imageOrientation == .up {
      return self
    }

    UIGraphicsBeginImageContextWithOptions(size, false, scale)
    draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
    if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
      UIGraphicsEndImageContext()
      return normalizedImage
    }
    UIGraphicsEndImageContext()
    return nil
  }
}

// MARK: - Alpha

public extension UIImage {
  /// Returns whether the image contains an alpha component.
  public var afContainsAlphaComponent: Bool {
    let alphaInfo = cgImage?.alphaInfo

    return (
      alphaInfo == .first ||
        alphaInfo == .last ||
        alphaInfo == .premultipliedFirst ||
        alphaInfo == .premultipliedLast
    )
  }

  /// Returns whether the image is opaque.
  public var afIsOpaque: Bool { return !afContainsAlphaComponent }
}

// MARK: - Scaling

public extension UIImage {
  /// Returns a new version of the image scaled to the specified size.
  ///
  /// - parameter size: The size to use when scaling the new image.
  ///
  /// - returns: A new image object.
  public func af_imageScaled(to size: CGSize) -> UIImage {
    assert(size.width > 0 && size.height > 0, "You cannot safely scale an image to a zero width or height")

    UIGraphicsBeginImageContextWithOptions(size, afIsOpaque, 0.0)
    draw(in: CGRect(origin: .zero, size: size))

    let scaledImage = UIGraphicsGetImageFromCurrentImageContext() ?? self
    UIGraphicsEndImageContext()

    return scaledImage
  }

  /// Returns a new version of the image scaled from the center while maintaining the aspect ratio to fit within
  /// a specified size.
  ///
  /// The resulting image contains an alpha component used to pad the width or height with the necessary transparent
  /// pixels to fit the specified size. In high performance critical situations, this may not be the optimal approach.
  /// To maintain an opaque image, you could compute the `scaledSize` manually, then use the `af_imageScaledToSize`
  /// method in conjunction with a `.Center` content mode to achieve the same visual result.
  ///
  /// - parameter size: The size to use when scaling the new image.
  ///
  /// - returns: A new image object.
  public func af_imageAspectScaled(toFit size: CGSize) -> UIImage {
    assert(size.width > 0 && size.height > 0, "You cannot safely scale an image to a zero width or height")

    let imageAspectRatio = self.size.width / self.size.height
    let canvasAspectRatio = size.width / size.height

    var resizeFactor: CGFloat

    if imageAspectRatio > canvasAspectRatio {
      resizeFactor = size.width / self.size.width
    } else {
      resizeFactor = size.height / self.size.height
    }

    let scaledSize = CGSize(width: self.size.width * resizeFactor, height: self.size.height * resizeFactor)
    let origin = CGPoint(x: (size.width - scaledSize.width) / 2.0, y: (size.height - scaledSize.height) / 2.0)

    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    draw(in: CGRect(origin: origin, size: scaledSize))

    let scaledImage = UIGraphicsGetImageFromCurrentImageContext() ?? self
    UIGraphicsEndImageContext()

    return scaledImage
  }

  /// Returns a new version of the image scaled from the center while maintaining the aspect ratio to fill a
  /// specified size. Any pixels that fall outside the specified size are clipped.
  ///
  /// - parameter size: The size to use when scaling the new image.
  ///
  /// - returns: A new image object.
  public func af_imageAspectScaled(toFill size: CGSize) -> UIImage {
    assert(size.width > 0 && size.height > 0, "You cannot safely scale an image to a zero width or height")

    let imageAspectRatio = self.size.width / self.size.height
    let canvasAspectRatio = size.width / size.height

    var resizeFactor: CGFloat

    if imageAspectRatio > canvasAspectRatio {
      resizeFactor = size.height / self.size.height
    } else {
      resizeFactor = size.width / self.size.width
    }

    let scaledSize = CGSize(width: self.size.width * resizeFactor, height: self.size.height * resizeFactor)
    let origin = CGPoint(x: (size.width - scaledSize.width) / 2.0, y: (size.height - scaledSize.height) / 2.0)

    UIGraphicsBeginImageContextWithOptions(size, afIsOpaque, 0.0)
    draw(in: CGRect(origin: origin, size: scaledSize))

    let scaledImage = UIGraphicsGetImageFromCurrentImageContext() ?? self
    UIGraphicsEndImageContext()

    return scaledImage
  }
}

// MARK: - Rounded Corners

public extension UIImage {
  /// Returns a new version of the image with the corners rounded to the specified radius.
  ///
  /// - parameter radius:                   The radius to use when rounding the new image.
  /// - parameter divideRadiusByImageScale: Whether to divide the radius by the image scale. Set to `true` when the
  ///                                       image has the same resolution for all screen scales such as @1x, @2x and
  ///                                       @3x (i.e. single image from web server). Set to `false` for images loaded
  ///                                       from an asset catalog with varying resolutions for each screen scale.
  ///                                       `false` by default.
  ///
  /// - returns: A new image object.
  public func af_imageRounded(withCornerRadius radius: CGFloat, divideRadiusByImageScale: Bool = false) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)

    let scaledRadius = divideRadiusByImageScale ? radius / scale : radius

    let clippingPath = UIBezierPath(roundedRect: CGRect(origin: CGPoint.zero, size: size), cornerRadius: scaledRadius)
    clippingPath.addClip()

    draw(in: CGRect(origin: CGPoint.zero, size: size))

    let roundedImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    return roundedImage
  }

  /// Returns a new version of the image rounded into a circle.
  ///
  /// - returns: A new image object.
  public func af_imageRoundedIntoCircle() -> UIImage {
    let radius = min(size.width, size.height) / 2.0
    var squareImage = self

    if size.width != size.height {
      let squareDimension = min(size.width, size.height)
      let squareSize = CGSize(width: squareDimension, height: squareDimension)
      squareImage = af_imageAspectScaled(toFill: squareSize)
    }

    UIGraphicsBeginImageContextWithOptions(squareImage.size, false, 0.0)

    let clippingPath = UIBezierPath(
      roundedRect: CGRect(origin: CGPoint.zero, size: squareImage.size),
      cornerRadius: radius
    )

    clippingPath.addClip()

    squareImage.draw(in: CGRect(origin: CGPoint.zero, size: squareImage.size))

    let roundedImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()

    return roundedImage
  }
}

extension UIImage {
//  /// EZSE: Returns base64 string
//  public var base64: String {
//    return UIImageJPEGRepresentation(self, 1.0)!.base64EncodedString()
//  }
//
//  /// EZSE: Returns compressed image to rate from 0 to 1
//  public func compressImage(rate: CGFloat) -> Data? {
//    return UIImageJPEGRepresentation(self, rate)
//  }
//
//  /// EZSE: Returns Image size in Bytes
//  public func getSizeAsBytes() -> Int {
//    return UIImageJPEGRepresentation(self, 1)?.count ?? 0
//  }
//
//  /// EZSE: Returns Image size in Kylobites
//  public func getSizeAsKilobytes() -> Int {
//    let sizeAsBytes = getSizeAsBytes()
//    return sizeAsBytes != 0 ? sizeAsBytes / 1024 : 0
//  }
//
//  /// EZSE: scales image
//  public class func scaleTo(image: UIImage, w: CGFloat, h: CGFloat) -> UIImage {
//    let newSize = CGSize(width: w, height: h)
//    UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
//    image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
//    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//    UIGraphicsEndImageContext()
//    return newImage
//  }
//
//  /// EZSE Returns resized image with width. Might return low quality
//  public func resizeWithWidth(_ width: CGFloat) -> UIImage {
//    let aspectSize = CGSize(width: width, height: aspectHeightForWidth(width))
//
//    UIGraphicsBeginImageContext(aspectSize)
//    draw(in: CGRect(origin: CGPoint.zero, size: aspectSize))
//    let img = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//
//    return img!
//  }
//
//  /// EZSE Returns resized image with height. Might return low quality
//  public func resizeWithHeight(_ height: CGFloat) -> UIImage {
//    let aspectSize = CGSize(width: aspectWidthForHeight(height), height: height)
//
//    UIGraphicsBeginImageContext(aspectSize)
//    draw(in: CGRect(origin: CGPoint.zero, size: aspectSize))
//    let img = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//
//    return img!
//  }

  /// EZSE:
//  public func aspectHeightForWidth(_ width: CGFloat) -> CGFloat {
//    return (width * size.height) / size.width
//  }

  /// EZSE:
//  public func aspectWidthForHeight(_ height: CGFloat) -> CGFloat {
//    return (height * size.width) / size.height
//  }

  /// EZSE: Returns cropped image from CGRect
  public func croppedImage(_ bound: CGRect) -> UIImage? {
    guard size.width > bound.origin.x else {
      print("EZSE: Your cropping X coordinate is larger than the image width")
      return nil
    }
    guard size.height > bound.origin.y else {
      print("EZSE: Your cropping Y coordinate is larger than the image height")
      return nil
    }
    let scaledBounds: CGRect = CGRect(x: bound.x * scale, y: bound.y * scale, width: bound.w * scale, height: bound.h * scale)
    let imageRef = cgImage?.cropping(to: scaledBounds)

    let croppedImage: UIImage = UIImage(cgImage: imageRef!, scale: scale, orientation: UIImageOrientation.up)
    return croppedImage
  }

  /// EZSE: Use current image for pattern of color
//  public func withColor(_ tintColor: UIColor) -> UIImage {
//    UIGraphicsBeginImageContextWithOptions(size, false, scale)
//
//    let context = UIGraphicsGetCurrentContext()
//    context?.translateBy(x: 0, y: size.height)
//    context?.scaleBy(x: 1.0, y: -1.0)
//    context?.setBlendMode(CGBlendMode.normal)
//
//    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height) as CGRect
//    context?.clip(to: rect, mask: cgImage!)
//    tintColor.setFill()
//    context?.fill(rect)
//
//    let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
//    UIGraphicsEndImageContext()
//
//    return newImage
//  }

  /// EZSE: Returns the image associated with the URL
//  public convenience init?(urlString: String) {
//    guard let url = URL(string: urlString) else {
//      self.init(data: Data())
//      return
//    }
//    guard let data = try? Data(contentsOf: url) else {
//      print("EZSE: No image in URL \(urlString)")
//      self.init(data: Data())
//      return
//    }
//    self.init(data: data)
//  }
//
//  /// EZSE: Returns an empty image //TODO: Add to readme
//  public class func blankImage() -> UIImage {
//    UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1), false, 0.0)
//    let image = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//    return image!
//  }
}
