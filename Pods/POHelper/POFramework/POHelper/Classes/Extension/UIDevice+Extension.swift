/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 8/5/17.
 */

import UIKit

public extension UIDevice {
  public var isX: Bool {
    return UIScreen.main.bounds.height == 812
  }
}
