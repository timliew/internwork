//
//  String+Extension.swift
//  PinOn
//
//  Created by Kefei Qian on 12/1/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import Foundation

public extension String {
  public func matchRegex(_ regexPattern: String) -> [String] {
    do {
      let regex = try NSRegularExpression(pattern: regexPattern)
      let results = regex.matches(in: self,
                                  range: NSRange(startIndex..., in: self))
      return results.map {
        String(self[Range($0.range, in: self)!])
      }
    } catch let error {
      print("invalid regex: \(error.localizedDescription)")
      return []
    }
  }

  public func isValidEmail() -> Bool {
    let regexPattern = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"

    guard let regex = try? NSRegularExpression(pattern: regexPattern, options: []) else {
      return false
    }

    let matches = regex.matches(in: self, options: [], range: NSRange(location: 0, length: count))

    return matches.count > 0
  }
}

// MARK: - Crypto
