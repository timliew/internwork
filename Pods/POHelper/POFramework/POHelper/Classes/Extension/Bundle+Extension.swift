/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 8/5/17.
 */

import UIKit

public extension Bundle {
  public var releaseVersionNumber: String? {
    return infoDictionary?["CFBundleShortVersionString"] as? String
  }

  public var buildVersionNumber: String? {
    return infoDictionary?["CFBundleVersion"] as? String
  }
}
