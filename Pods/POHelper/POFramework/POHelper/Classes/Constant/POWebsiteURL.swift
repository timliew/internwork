/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 4/19/17.
 */

import UIKit

public struct POWebsiteURL {
  private init() {}

  private static let websiteDomain = "https://www.pinon.io/"

  public static let privacyPolicy = websiteDomain + "privacy"
  public static let terms = websiteDomain + "terms"

  public static let openSourceLibrary = websiteDomain + "open-source"
  public static let updateLog = websiteDomain + "update-log"
  public static let about = websiteDomain + "about"
}
