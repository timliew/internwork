/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 5/4/17.
 */

import UIKit

public struct POColor {
  private init() {}

  public static let black = UIColor(r: 40, g: 36, b: 33)
  public static let yellow = UIColor(r: 254, g: 224, b: 73)
  public static let green = UIColor(r: 82, g: 175, b: 82)
  public static let red = UIColor(r: 212, g: 73, b: 70)
  public static let white = UIColor(r: 255, g: 255, b: 255)

  public static func gray(_ degree: Int) -> UIColor {
    guard 0 <= degree, degree <= grayColorCollection.count else {
      fatalError("degree must between 0 ~ 8")
    }

    return grayColorCollection[degree]
  }

  private static let grayColorCollection: [UIColor] = [
    // 0
    UIColor(r: 255, g: 255, b: 255),

    // 1
    UIColor(r: 247, g: 247, b: 247),

    // 2
    UIColor(r: 238, g: 238, b: 238),

    // 3
    UIColor(r: 198, g: 198, b: 198),

    // 4
    UIColor(r: 170, g: 170, b: 170),

    // 5
    UIColor(r: 130, g: 130, b: 130),

    // 6
    UIColor(r: 87, g: 87, b: 87),

    // 7
    UIColor(r: 56, g: 56, b: 56),

    // 8
    UIColor(r: 0, g: 0, b: 0),
  ]

  static let placeholderGray = UIColor(r: 220, g: 220, b: 220)

  static let accentYellow = UIColor(r: 255, g: 206, b: 8)

  static let navigationBarBackgroundColor = white
  static let navigationBarItemLabelColor = black

  static let facebookBlue = UIColor(r: 50, g: 77, b: 143)
  static let facebookBlueHighlighted = UIColor(r: 45, g: 68, b: 127)

  static let googleWhite = UIColor(r: 255, g: 255, b: 255)
  static let googleWhiteHighlighted = gray(4)

  static let halloweenOrange = UIColor(r: 195, g: 89, b: 22)
  static let halloweenOrangeHighlighted = UIColor(r: 172, g: 79, b: 21)

  static let mentionBlue = UIColor(r: 33, g: 114, b: 207)

  static let profileBlue = UIColor(r: 203, g: 233, b: 250)

  static func getTopBarBackgroundColor() -> UIColor {
    return white
  }
}
