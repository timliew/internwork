/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 5/4/17.
 */

import UIKit

public struct POUI {
  private init() {}

  public static let screenWidth = UIScreen.main.bounds.width
  public static let screenHeight = UIScreen.main.bounds.height

  private static let screenBaseWidth: CGFloat = 414.0
  private static let screenBaseHeight: CGFloat = 736.0
//
//  private static let widthRatio = screenWidth / screenBaseWidth
//  private static let heightRatio = screenHeight / screenBaseHeight
//
//  static let statusBarHeight = UIApplication.shared.statusBarFrame.height
//
//  static let bottomBarHeight: CGFloat = 50
//
//  static let screenSize = CGSize(width: screenWidth, height: screenHeight)
//
//  static func height(_ h: CGFloat) -> CGFloat {
//    return screenHeight * h / screenBaseHeight
//  }
//
//  static func height(percentage: CGFloat) -> CGFloat {
//    return screenHeight * percentage / 100
//  }
//
  public static func width(_ width: CGFloat) -> CGFloat {
    return screenWidth * width / screenBaseWidth
  }
//
//  static func width(percentage: CGFloat) -> CGFloat {
//    return screenWidth * percentage / 100
//  }
//
//  static func percentage(width: CGFloat) -> CGFloat {
//    return width / screenWidth
//  }
//
//  static func percentage(height: CGFloat) -> CGFloat {
//    return height / screenHeight
//  }
//
//  static func fontSize(_ size: CGFloat) -> CGFloat {
//    return width(size)
//  }
//
//  enum SizeBase {
//    case width
//    case height
//  }
//
  public static func size(width w: CGFloat, height h: CGFloat) -> CGSize {
    return CGSize(width: width(w), height: width(h))
  }
//
  public static func size(_ size: CGFloat) -> CGFloat {
    return width(size)
  }
//
//  static func calculateCollectionViewCellWidth(collectionViewHorizontalMargin: CGFloat, cellsPerRow: Int, cellSpacing: CGFloat) -> CGFloat {
//    let width = (screenWidth - 2 * collectionViewHorizontalMargin - CGFloat(cellsPerRow - 1) * cellSpacing) / CGFloat(cellsPerRow)
//
//    return width
//  }
}
