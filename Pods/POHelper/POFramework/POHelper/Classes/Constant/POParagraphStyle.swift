//
//  PinOnParagraphStyle.swift
//  PinOn
//
//  Created by Kefei Qian on 10/2/17.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import UIKit

public struct POParagraphStyle {
  private init() {}

  public static let left: NSMutableParagraphStyle = {
    let style = NSMutableParagraphStyle()
    style.alignment = .left

    return style
  }()

  public static let center: NSMutableParagraphStyle = {
    let style = NSMutableParagraphStyle()
    style.alignment = .center

    return style
  }()

  public static let rightStyle: NSMutableParagraphStyle = {
    let style = NSMutableParagraphStyle()
    style.alignment = .right

    return style
  }()
}
