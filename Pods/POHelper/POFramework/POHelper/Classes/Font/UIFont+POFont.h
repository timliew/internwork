@interface UIFont (POFont)

+ (UIFont *)regularTextFontWithSize:(CGFloat)size;

+ (UIFont *)mediumTextFontWithSize:(CGFloat)size;

+ (UIFont *)semiboldTextFontWithSize:(CGFloat)size;

+ (UIFont *)boldTextFontWithSize:(CGFloat)size;

+ (UIFont *)regularDisplayFontWithSize:(CGFloat)size;

+ (UIFont *)mediumDisplayFontWithSize:(CGFloat)size;

+ (UIFont *)semiboldDisplayFontWithSize:(CGFloat)size;

+ (UIFont *)boldDisplayFontWithSize:(CGFloat)size;

@end
