#import "UIFont+POFont.h"
#import <CoreText/CoreText.h>

// Based on https://github.com/CocoaPods-Fonts/OpenSans/blob/874e65bc21abe54284e195484d2259b2fe858680/UIFont%2BOpenSans.m


@interface POFontLoader: NSObject

+ (void)loadFontWithFileName:(NSString *)fontFileName extension:(NSString *)extension;

@end

@implementation POFontLoader

+ (void)loadFontWithFileName:(NSString *)fontFileName extension:(NSString *)extension {
    NSBundle *bundle = [NSBundle bundleForClass:self];
    NSURL *fontURL = [bundle URLForResource:fontFileName withExtension:extension];
    NSData *fontData = [NSData dataWithContentsOfURL:fontURL];

    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)fontData);
    CGFontRef font = CGFontCreateWithDataProvider(provider);

    if (font) {
        CFErrorRef errorRef = NULL;
        if (CTFontManagerRegisterGraphicsFont(font, &errorRef) == NO) {
            NSError *error = (__bridge NSError *)errorRef;
            if (error.code == kCTFontManagerErrorAlreadyRegistered) {
                // nop - the font must have been registered by someone else already.
              NSLog(@"Font Already Registered");
            } else {
                @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:error.localizedDescription userInfo:@{ NSUnderlyingErrorKey: error }];
            }
        }

        CFRelease(font);
    } else {
      NSLog(@"Font cannot be created");
    }
  
    if (provider) {
        CFRelease(provider);
    }
}

@end

@implementation UIFont (POFont)

+ (instancetype)po_LoadAndReturnFont:(NSString *)fontName extension:(NSString *)extension size:(CGFloat)fontSize onceToken:(dispatch_once_t *)onceToken {
    // Overload to default to fontName for fontFileName
    return [self po_LoadAndReturnFont:fontName extension:extension size:fontSize onceToken:onceToken fontFileName:fontName];
}

+ (instancetype)po_LoadAndReturnFont:(NSString *)fontName extension:(NSString *)extension size:(CGFloat)fontSize onceToken:(dispatch_once_t *)onceToken fontFileName:(NSString *)fontFileName {

    dispatch_once(onceToken, ^{
        [POFontLoader loadFontWithFileName:fontFileName extension:extension];
    });

    return [self fontWithName:fontName size:fontSize];
}

+ (UIFont *)regularTextFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Text-Regular";
  NSString *type = @"otf";
  NSString *fontName = @"SFProText-Regular";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}

+ (UIFont *)regularDisplayFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Display-Regular";
  NSString *type = @"otf";
  NSString *fontName = @"SFProDisplay-Regular";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}

+ (UIFont *)semiboldTextFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Text-Semibold";
  NSString *type = @"otf";
  NSString *fontName = @"SFProText-Semibold";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}

+ (UIFont *)semiboldDisplayFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Display-Semibold";
  NSString *type = @"otf";
  NSString *fontName = @"SFProDisplay-Semibold";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}

+ (UIFont *)boldTextFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Text-Bold";
  NSString *type = @"otf";
  NSString *fontName = @"SFProText-Bold";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}

+ (UIFont *)boldDisplayFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Display-Bold";
  NSString *type = @"otf";
  NSString *fontName = @"SFProDisplay-Bold";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}

+ (UIFont *)mediumTextFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Text-Medium";
  NSString *type = @"otf";
  NSString *fontName = @"SFProText-Medium";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}

+ (UIFont *)mediumDisplayFontWithSize:(CGFloat)size {
  static dispatch_once_t onceToken;
  NSString *fontFile = @"SF-Pro-Display-Medium";
  NSString *type = @"otf";
  NSString *fontName = @"SFProDisplay-Medium";
  return [self po_LoadAndReturnFont:fontName extension:type size:size onceToken:&onceToken fontFileName:fontFile];
}


@end
