//
//  Router.swift
//  PinOn
//
//  Created by Kefei Qian on 11/13/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import UIKit

open class POResourceManager: NSObject {
  private static let cache = NSCache<NSString, UIImage>()

  open class func resourceBundle() -> Bundle {
    fatalError("Subclass need to override this function")
  }

  public class func imageForResource(_ name: String, cacheable: Bool = false) -> UIImage {
    if cacheable {
      if let cache = self.cache.object(forKey: name as NSString) {
        return cache
      }
    }

    let bundle = resourceBundle()
    let imagePath = bundle.path(forResource: name, ofType: "png", inDirectory: "images")
    let image = UIImage(contentsOfFile: imagePath!)!

    if cacheable {
      cache.setObject(image, forKey: name as NSString)
    }

    return image
  }
}
