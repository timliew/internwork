//
//  PinOnRequest.swift
//  PONetwork
//
//  Created by Kefei Qian on 2/2/18.
//  COPYRIGHT © 2018-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import Alamofire

public final class POHTTPRequestGenerator {

  // MARK: - Variables

//  static let sharedInstance = PORequestGenerator()

  // MARK: - Lifecycle

  private init() {}

  public static func generateRequest(with url: URL, method: HTTPMethod, headers: HTTPHeaders?, requestParams: Parameters?, timeOut: TimeInterval = 20) -> URLRequest {
    guard var request = try? URLRequest(url: url, method: method, headers: headers) else {
      fatalError("Cannot generate urlRequest")
    }

    request.setValue("application/json", forHTTPHeaderField: "Content-Type")

    if method != .get, let requestParams = requestParams {
      guard let bodyData = try? JSONSerialization.data(withJSONObject: requestParams, options: []) else {
        fatalError("Cannot generate requestParam data:\(requestParams)")
      }
      request.httpBody = bodyData
    }

    request.timeoutInterval = timeOut

    return request
  }
}
