//
//  POHTTPNetworkManager.swift
//  Pods
//
//  Created by Kefei Qian on 2/2/18.
//  COPYRIGHT © 2018-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import Alamofire
import Apollo
import CocoaLumberjack
import POCore
import SwiftyJSON

public final class POHTTPNetworkManager {
  public static let sharedInstance = POHTTPNetworkManager()

  private var sessionManager: SessionManager!

  private init() {
    var serverTrustPolicyManager: ServerTrustPolicyManager?

    if let domainName = POApp.app.graphQLEndpoint.host {
      guard let certificatePath = Bundle.main.path(forResource: domainName, ofType: "cer"),
        let certificateData = NSData(contentsOfFile: certificatePath),
        let certificate = SecCertificateCreateWithData(nil, certificateData) else {
        fatalError("Cannot find ssl certification \(domainName).cer")
      }
      let serverTrustPolicies: [String: ServerTrustPolicy] = [
        domainName: .pinCertificates(
          certificates: [certificate],
          validateCertificateChain: true,
          validateHost: true
        ),
      ]

      serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
    }

    sessionManager = SessionManager(serverTrustPolicyManager: serverTrustPolicyManager)

    let oauthHandler = POHTTPNetworkOAuthHandler()
    sessionManager.adapter = oauthHandler
    sessionManager.retrier = oauthHandler
  }

  public func request(_ request: URLRequest, completionHandler: @escaping (JSON?, Error?) -> Void) -> DataRequest {
    guard let sessionManager = sessionManager else {
      preconditionFailure("Please setup POHttpNetworkManager First")
    }

    let request = sessionManager.request(request).validate()
    request.responseJSON { response in
      if let error = response.error {
        DDLogError("Cannot fetch request:\(String(describing: response.request)) from server: \(error)")
        completionHandler(nil, PONetworkError.error(error))
        return
      }

      guard let data = response.data,
        let dataJSON = try? JSON(data: data) else {
        DDLogError("Cannot generate json data from request:\(String(describing: response.request)) from server")
        completionHandler(nil, PONetworkError.invalidResponse(response))
        return
      }

      completionHandler(dataJSON, nil)
    }

    return request
  }
}

extension DataRequest: Cancellable {}
