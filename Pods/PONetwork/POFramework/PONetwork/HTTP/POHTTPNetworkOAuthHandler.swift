
import Alamofire
import CocoaLumberjack
import POAuth
import RxSwift

enum POHTTPRequestHeaderKeyName: String {
  case userId = "user-id"
  case idToken = "id-token"
  case timeZone = "time-zone"
}

final class POHTTPNetworkOAuthHandler: RequestAdapter, RequestRetrier {
  private let lock = NSLock()

  private var requestsToRetry: [RequestRetryCompletion] = []

  private var isRefreshing = false

  private let disposeBag = DisposeBag()

  func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
    var urlRequest = urlRequest
    if let currentUser = POAuth.auth.currentUser {
      urlRequest.setValue(currentUser.accessToken, forHTTPHeaderField: POHTTPRequestHeaderKeyName.idToken.rawValue)
      urlRequest.setValue(currentUser.userId, forHTTPHeaderField: POHTTPRequestHeaderKeyName.userId.rawValue)
      urlRequest.setValue(TimeZone.current.identifier, forHTTPHeaderField: POHTTPRequestHeaderKeyName.timeZone.rawValue)
    }
    return urlRequest
  }

  func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
    lock.lock(); defer { lock.unlock() }
    if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
      requestsToRetry.append(completion)
      if !isRefreshing {
        isRefreshing = true
        POAuth.auth.currentUser?.refreshAccessToken()
          .asObservable()
          .subscribe { event in
            self.lock.lock(); defer { self.lock.unlock() }
            switch event {
            case .completed:
              self.isRefreshing = false
              self.requestsToRetry.removeAll()
            case let .next(isSuccess):
              self.requestsToRetry.forEach { $0(isSuccess, 0.0) }
            case let .error(error):
              DDLogError(error.localizedDescription)
              self.isRefreshing = false
              self.requestsToRetry.forEach { $0(false, 0.0) }
              self.requestsToRetry.removeAll()
            }
          }.disposed(by: disposeBag)
      }
    }
  }
}
