import RxSwift
import SwiftyJSON

public protocol POBaseAPIManager {
  associatedtype T
  func exec() -> Observable<T>
}
