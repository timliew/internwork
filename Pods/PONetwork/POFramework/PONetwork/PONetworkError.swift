//
//  PONetworkError.swift
//  Pods
//
//  Created by Kefei Qian on 2/8/18.
//  COPYRIGHT © 2018-PRESENT ___ORGANIZATIONNAME___ ALL RIGHTS RESERVED.
//

import Alamofire
import Foundation

public enum PONetworkError: Error {
  case invalidURL(_: URL)
  case error(_: Error)
  case invalidGraphQLResponse
  case invalidResponse(_: DataResponse<Any>)
  case invalidRequest(_: DataRequest)
}
