//
//  PinOnGraphQLNetworkManager.swift
//  PinOn
//
//  Created by Kefei Qian on 12/6/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import Alamofire
import Apollo
import POCore

public final class POGraphQLNetworkManager {
  public static let sharedInstance = POGraphQLNetworkManager()

  private var client: ApolloClient!

  // MARK: - Lifecycle

  private init() {
    let networkTransport = POGraphQLHTTPNetworkTransport(url: POApp.app.graphQLEndpoint)
    client = ApolloClient(networkTransport: networkTransport)
  }

  public func fetch<Query: GraphQLQuery>(query: Query, cachePolicy: CachePolicy = .fetchIgnoringCacheData, resultHandler: OperationResultHandler<Query>? = nil) -> Cancellable {
    guard let apollo = client else {
      preconditionFailure("Please setup apolloClient first")
    }

    return apollo.fetch(query: query, cachePolicy: cachePolicy, queue: DispatchQueue.global(qos: .background), resultHandler: resultHandler)
  }

  public func perform<Mutation: GraphQLMutation>(mutation: Mutation, resultHandler: OperationResultHandler<Mutation>? = nil) -> Cancellable {
    guard let apollo = client else {
      preconditionFailure("Please setup apolloClient first")
    }

    return apollo.perform(mutation: mutation, queue: DispatchQueue.global(qos: .background), resultHandler: resultHandler)
  }
}

// MARK: - Delegate
