//
//  PinOnHttpNetworkTransport.swift
//  PinOn
//
//  Created by Kefei Qian on 12/14/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import Alamofire
import Apollo
import SwiftyJSON

private enum POGraphQLError: Error {
  case parseResponseJSONError(JSON?)
}

/// A network transport that uses HTTP POST requests to send GraphQL operations to a server, and that uses `URLSession` as the networking implementation.
final class POGraphQLHTTPNetworkTransport: NSObject, NetworkTransport {
  private let url: URL
  private let serializationFormat = JSONSerializationFormat.self
  private let sendOperationIdentifiers: Bool

  /// Creates a network transport with the specified server URL and session configuration.
  ///
  /// - Parameters:
  ///   - url: The URL of a GraphQL server to connect to.
  ///   - configuration: A session configuration used to configure the session. Defaults to `URLSessionConfiguration.default`.
  ///   - sendOperationIdentifiers: Whether to send operation identifiers rather than full operation text, for use with servers that support query persistence. Defaults to false.
  public init(url: URL, sendOperationIdentifiers: Bool = false) {
    self.url = url
    self.sendOperationIdentifiers = sendOperationIdentifiers
  }

  /// Send a GraphQL operation to a server and return a response.
  ///
  /// - Parameters:
  ///   - operation: The operation to send.
  ///   - completionHandler: A closure to call when a request completes.
  ///   - response: The response received from the server, or `nil` if an error occurred.
  ///   - error: An error that indicates why a request failed, or `nil` if the request was succesful.
  /// - Returns: An object that can be used to cancel an in progress request.
  public func send<Operation>(operation: Operation, completionHandler: @escaping (GraphQLResponse<Operation>?, Error?) -> Void) -> Cancellable {
    let body = requestBody(for: operation)

    let headers: HTTPHeaders? = nil

    let request = POHTTPRequestGenerator.generateRequest(with: url, method: .post, headers: headers, requestParams: body.jsonObject)

    return POHTTPNetworkManager.sharedInstance.request(request) { response, error in
      if let error = error {
        completionHandler(nil, error)
        return
      }

      guard let body = response?.dictionaryObject else {
        completionHandler(nil, POGraphQLError.parseResponseJSONError(response))
        return
      }

      let graphQLResponse = GraphQLResponse(operation: operation, body: body)
      completionHandler(graphQLResponse, nil)
    }
  }

  private func requestBody<Operation: GraphQLOperation>(for operation: Operation) -> GraphQLMap {
    if sendOperationIdentifiers {
      guard let operationIdentifier = type(of: operation).operationIdentifier else {
        preconditionFailure("To send operation identifiers, Apollo types must be generated with operationIdentifiers")
      }
      return ["id": operationIdentifier, "variables": operation.variables]
    }
    return ["query": type(of: operation).requestString, "variables": operation.variables]
  }
}
