//
//  MainCoordinator.swift
//  PinOn
//
//  Created by Kefei Qian on 11/13/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import PONav

public protocol MainCoordinatorDelegate: class {
  func mainCoordinatorShouldDismiss(_ coordinator: MainCoordinator)

  func mainCoordinatorShouldShowFirstSection(_ coordinator: MainCoordinator, router: PORouter)
  func mainCoordinatorShouldShowSecondection(_ coordinator: MainCoordinator, router: PORouter)
  func mainCoordinatorShouldShowThirdSection(_ coordinator: MainCoordinator, router: PORouter)
  func mainCoordinatorShouldShowForthSection(_ coordinator: MainCoordinator, router: PORouter)
  func mainCoordinatorShouldShowFifthSection(_ coordinator: MainCoordinator, router: PORouter)
}

public final class MainCoordinator: POCoordinator {
  public weak var delegate: MainCoordinatorDelegate!

  private let router: PORouter

  public init(router: PORouter) {
    self.router = router
    DDLogVerbose("init")
    super.init(name: String(describing: MainCoordinator.self))
  }

  deinit {
    DDLogVerbose("deinit")
  }

  public override func start() {
    DispatchQueue.main.async {
      let module = MainViewController()
      module.router = self
      self.router.setRootModule(module, hideBar: true, animated: true)
    }
  }
}

extension MainCoordinator: MainWireframe {
  func showFirstSection(_ navigationController: ASNavigationController) {
    delegate.mainCoordinatorShouldShowFirstSection(self, router: PORouter(rootController: navigationController))
  }

  func showSecondSection(_ navigationController: ASNavigationController) {
    delegate.mainCoordinatorShouldShowSecondection(self, router: PORouter(rootController: navigationController))
  }

  func showThirdSection(_ navigationController: ASNavigationController) {
    delegate.mainCoordinatorShouldShowThirdSection(self, router: PORouter(rootController: navigationController))
  }

  func showForthSection(_ navigationController: ASNavigationController) {
    delegate.mainCoordinatorShouldShowForthSection(self, router: PORouter(rootController: navigationController))
  }

  func showFifthSection(_ navigationController: ASNavigationController) {
    delegate.mainCoordinatorShouldShowFifthSection(self, router: PORouter(rootController: navigationController))
  }
}
