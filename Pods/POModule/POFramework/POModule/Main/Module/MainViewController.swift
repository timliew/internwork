//
//  MainViewController.swift
//  PinOn
//
//  Created by Kefei Qian on 9/20/17.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import AsyncDisplayKit
import CocoaLumberjack
import POHelper
import SnapKit

public let POMainModuleShouldShowExploreModuleNotificationName = Notification.Name("POMainModuleShouldShowExploreModuleNotification")
public let POMainModuleShouldShowTopicModuleNotificationName = Notification.Name("POMainModuleShouldShowTopicModuleNotification")
public let POMainModuleShouldShowCreateModuleNotificationName = Notification.Name("POMainModuleShouldShowCreateModuleNotification")
public let POMainModuleShouldShowSearchModuleNotificationName = Notification.Name("POMainModuleShouldShowSearchModuleNotification")
public let POMainModuleShouldShowProfileModuleNotificationName = Notification.Name("POMainModuleShouldShowProfileModuleNotification")

public let POMainModuleShouldShowTabBarNotificationName = Notification.Name("POMainModuleShouldShowTabBarNotification")
public let POMainModuleShouldHideTabBarNotificationName = Notification.Name("POMainModuleShouldHideTabBarNotification")

let POMainModuleShouldShowNewNotificationIndicatorName = Notification.Name("PODidReceiveNewNotificationFromServer")
let POMainModuleShouldHideNewNotificationIndicatorName = Notification.Name("PODidReadNewNotificationFromServer")

protocol MainView {
  var router: MainWireframe! { get set }
}

protocol MainWireframe {
  func showFirstSection(_ navigationController: ASNavigationController)
  func showSecondSection(_ navigationController: ASNavigationController)
  func showThirdSection(_ navigationController: ASNavigationController)
  func showForthSection(_ navigationController: ASNavigationController)
  func showFifthSection(_ navigationController: ASNavigationController)
}

final class MainViewController: ASTabBarController, MainView {

  // MARK: - Properties

  var router: MainWireframe!

  // MARK: - Variables

  private let tabbarVCImages = [
    [
      MainResourceManager.exploreButtonInactiveImage(),
      MainResourceManager.exploreButtonActiveImage(),
    ],
    [
      MainResourceManager.topicButtonInactiveImage(),
      MainResourceManager.topicButtonActiveImage(),
    ],
    [
      MainResourceManager.createButtonNormalImage(),
      MainResourceManager.createButtonHighlightedImage(),
    ],
    [
      MainResourceManager.searchButtonInctiveImage(),
      MainResourceManager.searchButtonActiveImage(),
    ],
    [
      MainResourceManager.profileButtonInactiveImage(),
      MainResourceManager.profileButtonActiveImage(),
    ],
    [
      MainResourceManager.profileHasNotificationButtonInactiveImage(),
      MainResourceManager.profileHasNotificationButtonActiveImage(),
    ],
  ]

  private var hasSetupCenterButton = false
  private var isInitLoad = true

  // MARK: TabBar ViewControllers

  private lazy var exploreViewController: ASNavigationController = {
    let vc = ASNavigationController()
    vc.setNavigationBarHidden(true, animated: false)
    vc.tabBarItem = MainViewControllerTabBarItem(inactiveImage: tabbarVCImages[0][0],
                                                 activeImage: tabbarVCImages[0][1])
    return vc
  }()

  private lazy var topicViewController: ASNavigationController = {
    let vc = ASNavigationController()
    vc.setNavigationBarHidden(true, animated: false)
    vc.tabBarItem = MainViewControllerTabBarItem(inactiveImage: tabbarVCImages[1][0],
                                                 activeImage: tabbarVCImages[1][1])
    return vc
  }()

  private lazy var createViewController = ASNavigationController()

  private lazy var searchViewController: ASNavigationController = {
    let vc = ASNavigationController()
    vc.setNavigationBarHidden(true, animated: false)
    vc.tabBarItem = MainViewControllerTabBarItem(inactiveImage: tabbarVCImages[3][0],
                                                 activeImage: tabbarVCImages[3][1])
    return vc
  }()

  private lazy var profileViewController: ASNavigationController = {
    let vc = ASNavigationController()
    vc.setNavigationBarHidden(true, animated: false)
    return vc
  }()

  private var centerButton: UIButton!

  // MARK: - Lifecycle

  deinit {
    DDLogVerbose("deinit")
    unsubscribe()
  }

  // MARK: - View Lifecycle

  override func viewDidLoad() {
    DDLogVerbose("init")
    super.viewDidLoad()

    tabBar.tintColor = .white
    tabBar.barTintColor = .white
    tabBar.isTranslucent = false
    delegate = self

    view.backgroundColor = .white

    viewControllers = [
      exploreViewController,
      topicViewController,
      UIViewController(),
      searchViewController,
      profileViewController,
    ]

    subscribe()
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()

    if !hasSetupCenterButton {
      hasSetupCenterButton = true
      setupCenterButton()
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    if isInitLoad {
      isInitLoad = false
      showExploreMoudle()
    }

    if UIApplication.shared.applicationIconBadgeNumber == 0 {
      profileViewController.tabBarItem = MainViewControllerTabBarItem(inactiveImage: tabbarVCImages[4][0],
                                                                      activeImage: tabbarVCImages[4][1])
    } else {
      profileViewController.tabBarItem = MainViewControllerTabBarItem(inactiveImage: tabbarVCImages[5][0],
                                                                      activeImage: tabbarVCImages[5][1])
    }
  }

  // MARK: - Layout

  private func setupCenterButton() {
    let centerButtonNormalImage = tabbarVCImages[2][0]
    let centerButtonHighlightedImage = tabbarVCImages[2][1]

    centerButton = UIButton()

    centerButton.setBackgroundImage(centerButtonNormalImage, for: .normal)
    centerButton.setBackgroundImage(centerButtonHighlightedImage, for: .highlighted)

    centerButton.addTarget(self, action: #selector(didTapCreateButton), for: .touchUpInside)

    view.addSubview(centerButton)

    centerButton.snp.makeConstraints { maker in
      let centerButtonHeight: CGFloat = 76
      let centerButtonWidth: CGFloat = 72

      maker.width.equalTo(centerButtonWidth)
      maker.height.equalTo(centerButtonHeight)
      maker.centerX.equalTo(view)
      if UIDevice.current.isX {
        maker.centerY.equalTo(tabBar).offset(-30.5)
      } else {
        let heightDifference = centerButtonHeight - tabBar.frame.size.height
        maker.centerY.equalTo(tabBar).offset(-heightDifference / 2)
      }
    }
  }

  // MARK: - UI Interaction

  // MARK: - User Interaction

  @objc private func didTapCreateButton() {
    router.showThirdSection(createViewController)
  }

  // MARK: - Controller Logic

  // MARK: - Notifications

  private func subscribe() {
    NotificationCenter.default.addObserver(self, selector: #selector(showProfileModule), name: POMainModuleShouldShowProfileModuleNotificationName, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(showTabBar), name: POMainModuleShouldShowTabBarNotificationName, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(hideTabBar), name: POMainModuleShouldHideTabBarNotificationName, object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(showNewNotificationIndicator), name: POMainModuleShouldShowNewNotificationIndicatorName, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(hideNewNotificationIndicator), name: POMainModuleShouldHideNewNotificationIndicatorName, object: nil)
  }

  private func unsubscribe() {
    NotificationCenter.default.removeObserver(self)
  }

  @objc private func showExploreMoudle() {
    selectedIndex = 0
    router.showFirstSection(exploreViewController)
  }

  @objc private func showTopicMoudle() {
    selectedIndex = 1
    router.showFirstSection(topicViewController)
  }

  @objc private func showSearchMoudle() {
    selectedIndex = 3
    router.showFirstSection(searchViewController)
  }

  @objc private func showProfileModule() {
    selectedIndex = 4
    router.showFifthSection(profileViewController)
  }

  @objc private func showNewNotificationIndicator() {
    profileViewController.tabBarItem = MainViewControllerTabBarItem(inactiveImage: tabbarVCImages[5][0],
                                                                    activeImage: tabbarVCImages[5][1])
  }

  @objc private func hideNewNotificationIndicator() {
    profileViewController.tabBarItem = MainViewControllerTabBarItem(inactiveImage: tabbarVCImages[4][0],
                                                                    activeImage: tabbarVCImages[4][1])
  }

  @objc private func showTabBar() {
    hideCenterButton(hide: false)
  }

  @objc private func hideTabBar() {
    hideCenterButton(hide: true)
  }

  // MARK: - Helpers

  private func hideCenterButton(hide: Bool) {
    DispatchQueue.main.async {
      UIView.animate(withDuration: 0.3,
                     animations: {
                       if hide {
                         self.tabBar.transform = CGAffineTransform(translationX: 0, y: 120)
                         self.centerButton.transform = CGAffineTransform(translationX: 0, y: 120)
                       } else {
                         self.tabBar.transform = CGAffineTransform.identity
                         self.centerButton.transform = CGAffineTransform.identity
                       }
      })
    }
  }
}

// MARK: - UITabBarControllerDelegate

extension MainViewController: UITabBarControllerDelegate {
  func tabBarController(_: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    guard let sectionViewController = viewController as? ASNavigationController else {
      return false
    }

    switch viewController {
    case exploreViewController:
      router.showFirstSection(sectionViewController)
    case topicViewController:
      router.showSecondSection(sectionViewController)
    case createViewController:
      return false
    case searchViewController:
      router.showForthSection(sectionViewController)
    case profileViewController:
      router.showFifthSection(sectionViewController)
    default:
      break
    }

    return true
  }
}
