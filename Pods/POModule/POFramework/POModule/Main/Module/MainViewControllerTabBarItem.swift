//
//  MainViewControllerTabBarItem.swift
//  POFramework
//
//  Created by Kefei Qian on 3/1/18.
//  COPYRIGHT © 2018-PRESENT CocoaPods ALL RIGHTS RESERVED.
//

import UIKit

final class MainViewControllerTabBarItem: UITabBarItem {
  required init(title: String? = nil, inactiveImage: UIImage?, activeImage: UIImage?, offset: CGFloat? = nil) {
    super.init()

    self.title = title
    image = inactiveImage?.withRenderingMode(.alwaysOriginal)
    selectedImage = activeImage?.withRenderingMode(.alwaysOriginal)

    if let offset = offset {
      imageInsets = UIEdgeInsetsMake(offset, 0, -offset, 0)
    }
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
