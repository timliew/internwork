//
//  MainResourceManager.swift
//  POFramework
//
//  Created by Kefei Qian on 3/2/18.
//  COPYRIGHT © 2018-PRESENT CocoaPods ALL RIGHTS RESERVED.
//

import POHelper
import UIKit

public final class MainResourceManager: POResourceManager {
  public override class func resourceBundle() -> Bundle {
    let assetPath = Bundle(for: MainResourceManager.self).resourcePath!
    return Bundle(path: (assetPath as NSString).appendingPathComponent("MainResource.bundle"))!
  }

  // MARK: - Images

  public class func createButtonHighlightedImage() -> UIImage {
    return imageForResource("CreateButtonHighlighted", cacheable: true)
  }

  public class func createButtonNormalImage() -> UIImage {
    return imageForResource("CreateButtonNormal", cacheable: true)
  }

  public class func topicButtonActiveImage() -> UIImage {
    return imageForResource("TopicButtonActive", cacheable: true)
  }

  public class func topicButtonInactiveImage() -> UIImage {
    return imageForResource("TopicButtonInactive", cacheable: true)
  }

  public class func exploreButtonActiveImage() -> UIImage {
    return imageForResource("ExploreButtonActive", cacheable: true)
  }

  public class func exploreButtonInactiveImage() -> UIImage {
    return imageForResource("ExploreButtonInactive", cacheable: true)
  }

  public class func profileButtonActiveImage() -> UIImage {
    return imageForResource("ProfileButtonActive", cacheable: true)
  }

  public class func searchButtonActiveImage() -> UIImage {
    return imageForResource("SearchButtonActive", cacheable: true)
  }

  public class func searchButtonInctiveImage() -> UIImage {
    return imageForResource("SearchButtonInctive", cacheable: true)
  }

  public class func profileButtonInactiveImage() -> UIImage {
    return imageForResource("ProfileButtonInactive", cacheable: true)
  }

  public class func profileHasNotificationButtonActiveImage() -> UIImage {
    return imageForResource("ProfileHasNotificationButtonActive", cacheable: true)
  }

  public class func profileHasNotificationButtonInactiveImage() -> UIImage {
    return imageForResource("ProfileHasNotificationButtonInactive", cacheable: true)
  }
}
