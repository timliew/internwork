//
//  AuthTextField.swift
//  POFramework
//
//  Created by Kefei Qian on 2/28/18.
//  COPYRIGHT © 2018-PRESENT CocoaPods ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import POHelper

final class AuthTextField: ASEditableTextNode {

  // MARK: - Variables

  enum Status {
    case clear
    case success
    case danger
  }

  // MARK: - View Components

  // MARK: - Lifecycle

  convenience init(placeholder: String, height: CGFloat) {
    self.init()

    attributedPlaceholderText = NSAttributedString(string: placeholder, attributes: [
      NSAttributedStringKey.foregroundColor: UIColor(r: 130, g: 130, b: 130),
      NSAttributedStringKey.font: UIFont.regularTextFont(withSize: 16),
    ])

    typingAttributes = [
      NSAttributedStringKey.foregroundColor.rawValue: POColor.black,
      NSAttributedStringKey.font.rawValue: UIFont.regularTextFont(withSize: 16),
    ]

    textContainerInset = UIEdgeInsets(top: (height - textView.contentSize.height * textView.zoomScale) / 2, left: POUI.size(12), bottom: 0, right: POUI.size(12))

    textView.textContainer.maximumNumberOfLines = 1
    textView.textContainer.lineBreakMode = .byTruncatingMiddle

    backgroundColor = .white

    cornerRadius = 4.0

    clipsToBounds = true

    automaticallyManagesSubnodes = true

    style.preferredLayoutSize = ASLayoutSize(width: ASDimension(unit: .fraction, value: 1), height: ASDimension(unit: .points, value: POUI.size(50)))
  }

  // MARK: - View Lifecycle

  // MARK: - Layout

  // MARK: - UI Interaction

  // MARK: - User Interaction

  // MARK: - Controller Logic

  // MARK: - Notifications

  // MARK: - Helpers

  func setPlaceholder(_ placeholder: String) {
    attributedPlaceholderText = NSAttributedString(string: placeholder, attributes: [
      NSAttributedStringKey.foregroundColor: UIColor(r: 130, g: 130, b: 130),
      NSAttributedStringKey.font: UIFont.regularTextFont(withSize: 16),
    ])
  }

  func setText(_ text: String) {
    attributedText = NSAttributedString(string: text, attributes: [
      NSAttributedStringKey.foregroundColor: POColor.black,
      NSAttributedStringKey.font: UIFont.regularTextFont(withSize: 16),
    ])
  }

  func setStatus(_ status: Status) {
    switch status {
    case .clear:
      borderWidth = 0
      borderColor = UIColor.clear.cgColor
    case .success:
      borderWidth = 1
      borderColor = POColor.green.cgColor
    case .danger:
      borderWidth = 1
      borderColor = POColor.red.cgColor
    }
  }
}
