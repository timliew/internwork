//
//  AuthText.swift
//  POFramework
//
//  Created by Kefei Qian on 2/28/18.
//  COPYRIGHT © 2018-PRESENT CocoaPods ALL RIGHTS RESERVED.
//

import Foundation

struct AuthText {
  struct Error {
    static let googleError = NSLocalizedString("Oops, there is something wrong with Google server, please try again later.", comment: "when we cannot fetch google id token")
    static let facebookError = NSLocalizedString("Oops, there is something wrong with Facebook server, please try again later.", comment: "when we cannot fetch google id token")
    static let passwordLengthIncorrect = NSLocalizedString("Password should contain 6 to 32 characters.", comment: "If the password user input less than 8 character, this message will show")
    static let usernameLengthIncorrect = NSLocalizedString("Username should contain 6 to 32 characters.", comment: "If the username user input less than 8 character, this message will show")
    static let usernamePasswordDoNotMatch = NSLocalizedString("Username and Password do not match, please try again.", comment: "The username and password user inputed do not match")

    static let usernameUnavailable = NSLocalizedString("The Username is unavaliable, please try another.", comment: "Username is taken")
    static let usernameInvalidFormat = NSLocalizedString("The format of the Username is invalid, please try another.", comment: "Username format inivalid")
    static let emailUnavailable = NSLocalizedString("The Email is already taken, please try another.", comment: "Email is taken")
    static let emailInvalidFormat = NSLocalizedString("The Email you entered is not valid, please try again.", comment: "Email format invalid")

    static let emailEmpty = NSLocalizedString("Please enter an Email.", comment: "email empty")
    static let usernameEmpty = NSLocalizedString("Please enter an Username.", comment: "username empty")
    static let passwordEmpty = NSLocalizedString("Please enter a Password.", comment: "password empty")

    static let resetPasswordUserNotFound = NSLocalizedString("We cannot find an account associated with the info you entered.", comment: "reset password user not found")

    static let needResetPassword = NSLocalizedString("Your Password needs to be updated. Please reset your password", comment: "need reset password")
  }

  struct AuthViewController {
    static let introPage1Description = NSLocalizedString("A map-based content sharing platform for travelers.", comment: "IntroPage 1 description")
    static let introPage2Description = NSLocalizedString("Pin your travel moments on a map using \"Emoji Pins\".", comment: "IntroPage 2 description")

    static let facebookSignUpButtonTitle = NSLocalizedString("Sign up with Facebook", comment: "FacebookSignUpButton title")
    static let facebookLoginButtonTitle = NSLocalizedString("Login with Facebook", comment: "FacebookLoginButton title")

    static let googleSignUpButtonTitle = NSLocalizedString("Sign up with Google", comment: "GoogleSignUpButton title")
    static let googleLoginButtonTitle = NSLocalizedString("Login with Google", comment: "GoogleLoginButton title")

    static let signUpButtonTitle = NSLocalizedString("Sign up", comment: " SignUpButton title")
    static let loginButtonTitle = NSLocalizedString("Login", comment: " LoginButton title")

    static let switchModeSignUpButtonTitle = NSLocalizedString("Already have an account?", comment: "SwitchModeButton SignUp title")
    static let switchModeLoginButtonTitle = NSLocalizedString("I don’t have an account.", comment: "SwitchModeButton Login title")

    static let forgotPasswordButtonTitle = NSLocalizedString("Forgot Password?", comment: "ForgotPasswordButton title")

    static let usernameSignUpTextFieldPlaceholder = NSLocalizedString("Choose a Username", comment: "UsernameTextField placeholder title")
    static let usernameLoginTextFieldPlaceholder = NSLocalizedString("Username or Email", comment: "UsernameTextField placeholder title")

    static let emailTextFieldPlaceholder = NSLocalizedString("Choose an Email", comment: "EmailTextField placeholder title")
    static let passwordLoginTextFieldPlaceholder = NSLocalizedString("Password", comment: "PasswordTextField placeholder title")
    static let passwordSignUpTextFieldPlaceholder = NSLocalizedString("Choose a Password", comment: "PasswordTextField placeholder title")

    private static let preText = "By signing up, you agree to our"
    private static let terms = "Terms"
    private static let and = "&"
    private static let privacyPolicy = "Privacy Policy"

    private static let signUpTermPrivacyTextPreText = NSLocalizedString(preText, comment: "By signing up, you agree to our")
    static let signUpTermPrivacyTextTerms = NSLocalizedString(terms, comment: "Terms")
    private static let signUpTermPrivacyTextAnd = NSLocalizedString(and, comment: "and")
    static let signUpTermPrivacyTextPrivacyPolicy = NSLocalizedString(privacyPolicy, comment: "Privacy Policy")

    static let signUpTermPrivacyText = "\(preText) \(terms) \(and) \(privacyPolicy)"
  }

  struct AuthForgotPasswordViewController {
    static let resetPasswordTitle = NSLocalizedString("Reset your password", comment: "Reset password title")
    static let submit = NSLocalizedString("Submit", comment: "Submit")

    static let resetPasswordDescriptionText = NSLocalizedString("Please enter the Username or Email associate with your account. We will send you an email to help reset your password.", comment: "Reset password description")

    static let usernameEditableTextFieldPlaceholder = NSLocalizedString("Username or Email", comment: "UsernameTextField placeholder title")

    static let resetPasswordSentTitle = NSLocalizedString("Email sent.", comment: "resetPassword email sent title")
    static let resetPasswordSentDescription = NSLocalizedString("Please check your email box and follow the steps to reset your password.", comment: "resetPassword email sent description")
    static let resetPasswordSentButtonText = NSLocalizedString("OK", comment: "resetPassword email sent button text")
  }
}
