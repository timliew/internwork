import MBProgressHUD
import UIKit

extension UIViewController {
  func startAuthActivity() {
    DispatchQueue.main.async {
      let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
      hud.mode = .indeterminate
      hud.contentColor = .white
      hud.bezelView.backgroundColor = UIColor(r: 0, g: 0, b: 0, a: 80)
    }
  }

  func endAuthActivity() {
    DispatchQueue.main.async {
      MBProgressHUD.hide(for: self.view, animated: true)
    }
  }

  func showAuthMessage(_ message: Message?, time: TimeInterval = 2.5) {
    DispatchQueue.main.async {
      let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
      hud.mode = .text
      hud.hide(animated: true, afterDelay: time)
      hud.label.text = message
      hud.label.numberOfLines = 0
      hud.contentColor = .white
      hud.bezelView.backgroundColor = UIColor(r: 0, g: 0, b: 0, a: 80)
    }
  }
}
