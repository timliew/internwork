//
//  AuthButton.swift
//  POFramework
//
//  Created by Kefei Qian on 2/28/18.
//  COPYRIGHT © 2018-PRESENT CocoaPods ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import POHelper

final class AuthButton: ASButtonNode {

  // MARK: - Variables

  private let buttonTitle: String
  private let buttonTextColor: UIColor
  private let buttonTextFont: UIFont
  private let buttonIcon: UIImage?
  private let buttonBorderWidth: CGFloat
  private let buttonBorderColor: UIColor?
  private var buttonBackgroundColor: UIColor = POColor.white

  private var isGradiant: Bool = false

  // MARK: - View Components

  // MARK: - Lifecycle

  required init(title: String,
                textColor: UIColor = POColor.black,
                textFont: UIFont = UIFont.mediumTextFont(withSize: 16),
                icon: UIImage? = nil,
                borderWidth: CGFloat = 0,
                borderColor: UIColor? = nil,
                fill: UIColor = .clear,
                fillHighlighted: UIColor? = nil,
                backgroundColor: UIColor = .white,
                size: CGSize) {
    buttonTitle = title
    buttonTextColor = textColor
    buttonTextFont = textFont
    buttonIcon = icon
    buttonBorderWidth = borderWidth
    buttonBorderColor = borderColor
    buttonBackgroundColor = fill

    super.init()

    self.borderColor = borderColor?.cgColor
    self.borderWidth = borderWidth

    if let fillHighlighted = fillHighlighted {
      setBackgroundImage(UIImage(color: fill)?.af_imageScaled(to: size).af_imageRounded(withCornerRadius: size.height / 2), for: .normal)
      setBackgroundImage(UIImage(color: fillHighlighted)?.af_imageScaled(to: size).af_imageRounded(withCornerRadius: size.height / 2), for: .highlighted)
    } else {
      setBackgroundImage(UIImage(color: fill, size: size)?.af_imageRounded(withCornerRadius: size.height / 2), for: .normal)
    }

    setTitle(title, with: textFont, with: textColor, for: .normal)

    if let icon = icon {
      setImage(icon, for: .normal)
      imageNode.isOpaque = true
      imageNode.backgroundColor = fill
    }

    clipsToBounds = true

    isOpaque = true
    backgroundImageNode.isOpaque = true
    backgroundImageNode.backgroundColor = backgroundColor
  }

  convenience init(title: String, gradiant: Bool, backgroundColor: UIColor = .white, size: CGSize) {
    self.init(title: title, backgroundColor: backgroundColor, size: size)

    if gradiant {
      let normalBackgroundImage = AuthResourceManager.authButtonBackground().af_imageScaled(to: size).af_imageRounded(withCornerRadius: size.height / 2)
      let highlightedBackgroundImage = AuthResourceManager.authButtonHighlightedBackground().af_imageScaled(to: size).af_imageRounded(withCornerRadius: size.height / 2)

      setBackgroundImage(normalBackgroundImage, for: .normal)
      setBackgroundImage(highlightedBackgroundImage, for: .highlighted)
    }
  }

  // MARK: - Helpers

  func changeTitle(title: String) {
    setTitle(title, with: buttonTextFont, with: buttonTextColor, for: .normal)
  }

  func disable() {
    isEnabled = false
    setTitle(buttonTitle, with: buttonTextFont, with: POColor.gray(3), for: .normal)
  }

  func enable() {
    isEnabled = true
    setTitle(buttonTitle, with: buttonTextFont, with: buttonTextColor, for: .normal)
  }
}
