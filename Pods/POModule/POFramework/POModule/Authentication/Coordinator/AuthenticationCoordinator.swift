//
//  AuthCoordinator.swift
//  PinOn
//
//  Created by Kefei Qian on 11/15/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import PONav

public protocol AuthenticationCoordinatorDelegate: class {
  func authCoordinatorDidLogin(_ coordinator: AuthenticationCoordinator)
  func authCoordinatorDidSignUp(_ coordinator: AuthenticationCoordinator)
}

public final class AuthenticationCoordinator: POCoordinator {
  public weak var delegate: AuthenticationCoordinatorDelegate?

  // MARK: - Properties

  private let router: PORouter

  // MARK: - Lifecycle

  public init(router: PORouter) {
    self.router = router
    DDLogVerbose("init")
    super.init(name: String(describing: AuthenticationCoordinator.self))
  }

  deinit {
    DDLogVerbose("deinit")
  }

  public override func start() {
    showAuthModule()
  }

  private func showAuthModule() {
    let view = AuthViewController()
    let interactor = AuthInteractor()
    let presenter = AuthPresenter()

    view.presenter = presenter

    interactor.output = presenter

    presenter.view = view
    presenter.interactor = interactor
    presenter.router = self

    router.setRootModule(view, hideBar: true, animated: true)
  }
}

extension AuthenticationCoordinator: AuthWireframe {
  func runResetPasswordFlow() {
    let module = AuthResetPasswordViewController()
    module.router = self
    router.push(module)
  }

  func authDidFinishLogin() {
    delegate?.authCoordinatorDidLogin(self)
  }

  func authDidFinishSignUp() {
    delegate?.authCoordinatorDidSignUp(self)
  }
}

extension AuthenticationCoordinator: AuthResetPasswordRouter {
  func authResetPasswordShouldDismiss() {
    router.popModule()
  }
}
