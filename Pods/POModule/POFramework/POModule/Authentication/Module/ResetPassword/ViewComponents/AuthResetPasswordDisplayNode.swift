//
//  AuthResetPasswordDisplayNode.swift
//  PinOn
//
//  Created by Kefei Qian on 11/20/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import POHelper

protocol AuthResetPasswordDisplayNodeView {
  weak var delegate: AuthResetPasswordDisplayNodeDelegate? { get set }
}

protocol AuthResetPasswordDisplayNodeDelegate: class {
  func didTapBackButton()
  func didTapSubmitButton(_ button: AuthButton, usernameEditableTextNode: AuthTextField, username: Username)
}

final class AuthResetPasswordDisplayNode: ASDisplayNode, AuthResetPasswordDisplayNodeView {
  weak var delegate: AuthResetPasswordDisplayNodeDelegate?

  // MARK: - View Components

  private let backgroundNode: ASImageNode
  private let backButton: ASButtonNode

  private var scrollNode: ASScrollNode & AuthResetPasswordScrollNodeView

  // MARK: - Lifecycle

  override init() {
    backgroundNode = ASImageNode()
    backButton = ASButtonNode()
    scrollNode = AuthResetPasswordScrollNode()
    super.init()

    automaticallyManagesSubnodes = true
    backgroundColor = .white
  }

  override func didLoad() {
    super.didLoad()

    scrollNode.delegate = self

    backgroundNode.image = AuthResourceManager.authResetPasswordBackground()
    backgroundNode.isOpaque = true

    backButton.setImage(AuthResourceManager.authResetPasswordBackButton(), for: .normal)
    backButton.imageNode.isOpaque = true
    backButton.imageNode.backgroundColor = UIColor(r: 255, g: 233, b: 54)
    backButton.style.preferredSize = POUI.size(width: 55, height: 45)
    backButton.addTarget(self, action: #selector(didTapBackButton), forControlEvents: .touchUpInside)
  }

  // MARK: - Layout

  override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
    let backbuttonInsetLayout = ASInsetLayoutSpec(insets: UIEdgeInsetsMake(20, 0, 0, 0), child: backButton)

    let backButtonRelativeLayout = ASRelativeLayoutSpec(horizontalPosition: .start, verticalPosition: .start, sizingOption: [], child: backbuttonInsetLayout)

    return ASBackgroundLayoutSpec(child: backButtonRelativeLayout, background: ASBackgroundLayoutSpec(child: scrollNode, background: backgroundNode))
  }

  @objc private func didTapBackButton() {
    delegate?.didTapBackButton()
  }
}

extension AuthResetPasswordDisplayNode: AuthResetPasswordScrollNodeDelegate {
  func didTapSubmitButton(_ button: AuthButton, usernameEditableTextNode: AuthTextField, username: Username) {
    delegate?.didTapSubmitButton(button, usernameEditableTextNode: usernameEditableTextNode, username: username)
  }
}

// MARK: - Delegate
