//
//  AuthResetPasswordScrollNode.swift
//  PinOn
//
//  Created by Kefei Qian on 11/16/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import POHelper

protocol AuthResetPasswordScrollNodeView {
  weak var delegate: AuthResetPasswordScrollNodeDelegate? { get set }
}

protocol AuthResetPasswordScrollNodeDelegate: class {
  func didTapSubmitButton(_ button: AuthButton, usernameEditableTextNode: AuthTextField, username: Username)
}

final class AuthResetPasswordScrollNode: ASScrollNode, AuthResetPasswordScrollNodeView {

  // MARK: - Variables

  weak var delegate: AuthResetPasswordScrollNodeDelegate?

  private var username: String? {
    return usernameEditableTextNode.attributedText?.string.trimmingCharacters(in: .whitespacesAndNewlines)
  }

  // MARK: - View Components

  private let titleTextNode: ASTextNode
  private let descriptionLabelNode: ASTextNode

  private let usernameEditableTextNode: AuthTextField
  private let submitButton: AuthButton

  // MARK: - Lifecycle

  override init() {
    titleTextNode = ASTextNode()
    descriptionLabelNode = ASTextNode()
    usernameEditableTextNode = AuthTextField(placeholder: AuthText.AuthForgotPasswordViewController.usernameEditableTextFieldPlaceholder, height: POUI.size(50))
    submitButton = AuthButton(title: AuthText.AuthForgotPasswordViewController.submit, textFont: UIFont.semiboldTextFont(withSize: 18), fill: .white, size: POUI.size(width: 160, height: 50))
    super.init()

    automaticallyManagesContentSize = true
    automaticallyManagesSubnodes = true

    subscribe()
  }

  deinit {
    unsubscribe()
  }

  override func didLoad() {
    super.didLoad()

    setupViews()
  }

  // MARK: - View Lifecycle

  // MARK: - Layout

  func setupViews() {
    view.delegate = self
    view.showsVerticalScrollIndicator = false
    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapScrollNode)))

    titleTextNode.attributedText = NSAttributedString(string: AuthText.AuthForgotPasswordViewController.resetPasswordTitle, attributes: [
      NSAttributedStringKey.foregroundColor: POColor.black,
      NSAttributedStringKey.font: UIFont.semiboldDisplay(withSize: 24),
    ])
    titleTextNode.isOpaque = true
    titleTextNode.backgroundColor = UIColor(r: 255, g: 231, b: 57)

    descriptionLabelNode.attributedText = NSAttributedString(string: AuthText.AuthForgotPasswordViewController.resetPasswordDescriptionText, attributes: [
      NSAttributedStringKey.foregroundColor: POColor.black,
      NSAttributedStringKey.font: UIFont.regularTextFont(withSize: 15),
      NSAttributedStringKey.paragraphStyle: POParagraphStyle.center,
    ])
    descriptionLabelNode.style.width = ASDimension(unit: .points, value: POUI.size(320))
    descriptionLabelNode.style.spacingBefore = POUI.size(16)
    descriptionLabelNode.isOpaque = true
    descriptionLabelNode.backgroundColor = UIColor(r: 255, g: 231, b: 54)

    usernameEditableTextNode.style.preferredSize = POUI.size(width: 320, height: 50)
    usernameEditableTextNode.style.spacingBefore = POUI.size(30)
    usernameEditableTextNode.delegate = self
    usernameEditableTextNode.accessibilityIdentifier = "ResetPasswordUsernameTextField"

    submitButton.style.preferredSize = POUI.size(width: 160, height: 50)
    submitButton.cornerRadius = POUI.size(25)
    submitButton.style.spacingBefore = POUI.size(60)
    submitButton.disable()
    submitButton.addTarget(self, action: #selector(didTapSubmitButton), forControlEvents: .touchUpInside)
    submitButton.accessibilityIdentifier = "ResetPasswordSubmitButton"
  }

  override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
    let stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .start, alignItems: .center, children: [
      self.titleTextNode,
      self.descriptionLabelNode,
      self.usernameEditableTextNode,
      self.submitButton,
    ])

    let stackInsetLayout = ASInsetLayoutSpec(insets: UIEdgeInsetsMake(POUI.size(120), 0, 0, 0), child: stackLayout)

    return stackInsetLayout
  }

  // MARK: - User Interaction

  @objc private func didTapScrollNode() {
    view.endEditing(true)
  }

  @objc private func didTapSubmitButton(_ button: AuthButton) {
    guard let username = username?.trimmingCharacters(in: .whitespacesAndNewlines).lowercased() else {
      usernameEditableTextNode.setStatus(.danger)
      usernameEditableTextNode.becomeFirstResponder()
      return
    }

    usernameEditableTextNode.resignFirstResponder()

    delegate?.didTapSubmitButton(button, usernameEditableTextNode: usernameEditableTextNode, username: username)
  }

  // MARK: - Controller Logic

  // MARK: - Notifications

  func subscribe() {
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }

  func unsubscribe() {
    NotificationCenter.default.removeObserver(self)
  }

  @objc private func keyboardWillShow(notification: NSNotification) {
    if let userInfo = notification.userInfo,
      let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height,
      let isCurrentApp = userInfo[UIKeyboardIsLocalUserInfoKey] as? Bool, isCurrentApp {
      view.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight + 20, right: 0)
      view.scrollRectToVisible(submitButton.frame, animated: true)
    }
  }

  @objc private func keyboardWillHide(notification _: NSNotification) {
    UIView.animate(withDuration: 0.2, animations: {
      self.view.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    })
  }

  // MARK: - Helpers

}

extension AuthResetPasswordScrollNode: ASEditableTextNodeDelegate {
  func editableTextNodeDidUpdateText(_ editableTextNode: ASEditableTextNode) {
    if let textNode = editableTextNode as? AuthTextField {
      textNode.setStatus(.clear)
    }

    if let count = editableTextNode.attributedText?.string.trimmingCharacters(in: .whitespacesAndNewlines).count, count != 0 {
      submitButton.enable()
    } else {
      submitButton.disable()
    }
  }

  func editableTextNode(_ editableTextNode: ASEditableTextNode, shouldChangeTextIn _: NSRange, replacementText text: String) -> Bool {
    if text == "\n" {
      editableTextNode.resignFirstResponder()
      return false
    }

    return true
  }
}

extension AuthResetPasswordScrollNode: UIScrollViewDelegate {
  func scrollViewWillBeginDragging(_: UIScrollView) {
    view.endEditing(true)
  }
}
