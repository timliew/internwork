//
//  AuthForgotPasswordViewController.swift
//  PinOn
//
//  Created by Kefei Qian on 11/16/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import POAuth
import RxSwift

protocol AuthResetPasswordView: class {
  var router: AuthResetPasswordRouter! { get set }
}

protocol AuthResetPasswordRouter: class {
  func authResetPasswordShouldDismiss()
}

final class AuthResetPasswordViewController: ASViewController<ASDisplayNode>, AuthResetPasswordView {

  // MARK: - Variables

  var router: AuthResetPasswordRouter!

  // MARK: - View Components

  private var rootNode: ASDisplayNode & AuthResetPasswordDisplayNodeView

  private let disposeBag = DisposeBag()

  // MARK: - Lifecycle

  required init() {
    rootNode = AuthResetPasswordDisplayNode()
    super.init(node: rootNode)
    DDLogVerbose("init")

    node.automaticallyManagesSubnodes = true
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    DDLogVerbose("deinit")
  }

  // MARK: - View Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()

    rootNode.delegate = self
  }

  // MARK: - Layout

  override var prefersStatusBarHidden: Bool {
    return true
  }

  // MARK: - UI Interaction

  // MARK: - User Interaction

  // MARK: - Controller Logic

  // MARK: - Notifications

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    DDLogWarn("Memory low")
  }

  // MARK: - Helpers

  private func alertUserToDismissViewController() {
    let alertViewController = UIAlertController(title: AuthText.AuthForgotPasswordViewController.resetPasswordSentTitle, message: AuthText.AuthForgotPasswordViewController.resetPasswordSentDescription, preferredStyle: UIAlertControllerStyle.alert)

    let action = UIAlertAction(title: AuthText.AuthForgotPasswordViewController.resetPasswordSentButtonText, style: .default, handler: { action in
      self.router.authResetPasswordShouldDismiss()
    })

    alertViewController.addAction(action)

    DispatchQueue.main.async {
      self.present(alertViewController, animated: true, completion: nil)
    }
  }
}

extension AuthResetPasswordViewController: AuthResetPasswordDisplayNodeDelegate {
  func didTapBackButton() {
    router.authResetPasswordShouldDismiss()
  }

  func didTapSubmitButton(_: AuthButton, usernameEditableTextNode: AuthTextField, username: String) {
    startAuthActivity()

    POAuth.auth.resetPassword(of: username)
      .asObservable()
      .subscribe { event in
        self.endAuthActivity()
        switch event {
        case let .next(success):
          print("Reset Password: \(success)")
        case .completed:
//          self.showAuthMessage(AuthText.AuthForgotPasswordViewController.resetPasswordSentEmail)
          self.alertUserToDismissViewController()
        case let .error(error):
          switch error {
          case POAuthError.resetPasswordUserNotFound:
            self.showAuthMessage(AuthText.Error.resetPasswordUserNotFound)
          default:
            self.showAuthMessage(error.localizedDescription)
          }
        }
      }.disposed(by: disposeBag)
  }
}

// MARK: - ASEditableTextNodeDelegate
