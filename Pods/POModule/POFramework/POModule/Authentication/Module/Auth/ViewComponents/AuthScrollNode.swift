//
//  AuthScrollNode.swift
//  PinOn
//
//  Created by Kefei Qian on 11/15/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import POAuth
import POHelper

protocol AuthScrollNodeDelegate: class {
  func didTapForgotPasswordButton()
  func didTapLoginButton(username: Username, password: Password)
  func didTapSignUpButton(email: Email, username: Username, password: Password)
  func showAlert(_ message: Message)
}

protocol AuthScrollNodeView {
  weak var delegate: AuthScrollNodeDelegate? { get set }
  var state: AuthState { get set }
}

final class AuthScrollNode: ASScrollNode, AuthScrollNodeView {

  // MARK: - Variables

  weak var delegate: AuthScrollNodeDelegate?

  private var username: String? {
    return usernameEditableTextNode.attributedText?.string.trimmingCharacters(in: .whitespacesAndNewlines)
  }

  private var email: String? {
    return emailEditableTextNode.attributedText?.string.trimmingCharacters(in: .whitespacesAndNewlines)
  }

  private var password: String = ""

  var state: AuthState = .login(isExpanded: UIDevice.current.isX) {
    willSet {
      password = ""
    }
    didSet {
      switch state {
      case .login:
        switchModeButton.setTitle(AuthText.AuthViewController.switchModeLoginButtonTitle, with: UIFont.mediumTextFont(withSize: 14), with: POColor.black, for: .normal)
        authButton.changeTitle(title: AuthText.AuthViewController.loginButtonTitle)
        usernameEditableTextNode.setPlaceholder(AuthText.AuthViewController.usernameLoginTextFieldPlaceholder)
        passwordEditableTextNode.setPlaceholder(AuthText.AuthViewController.passwordLoginTextFieldPlaceholder)
      case .signUp:
        switchModeButton.setTitle(AuthText.AuthViewController.switchModeSignUpButtonTitle, with: UIFont.mediumTextFont(withSize: 14), with: POColor.black, for: .normal)
        authButton.changeTitle(title: AuthText.AuthViewController.signUpButtonTitle)
        usernameEditableTextNode.setPlaceholder(AuthText.AuthViewController.usernameSignUpTextFieldPlaceholder)
        passwordEditableTextNode.setPlaceholder(AuthText.AuthViewController.passwordSignUpTextFieldPlaceholder)
      }

      [emailEditableTextNode, usernameEditableTextNode, passwordEditableTextNode].forEach {
        $0.setStatus(.clear)
        $0.attributedText = nil
      }

      view.endEditing(true)

      transitionLayout(withAnimation: true, shouldMeasureAsync: false, measurementCompletion: nil)
    }
  }

  private let introPagerItem: [UIImage] = [
    AuthResourceManager.authIntroPagerNodeImage2(),
  ]

  // MARK: - View Components

  var introPagerNode: ASPagerNode
//  var introPageControl: UIPageControl

//  private lazy var introPageControlNode: ASDisplayNode = {
//    let node = ASDisplayNode(viewBlock: { [weak self] () -> UIView in
//      guard let pageControl = self?.introPageControl else {
//        return UIView()
//      }
//      return pageControl
//    })
//
//    return node
//  }()

  private let signUpTermPrivacyTextNode: ASTextNode

  private let authButton: AuthButton

  private let switchModeButton: ASButtonNode

  private let resetPasswordButton: ASButtonNode

  private let emailEditableTextNode: AuthTextField
  private let usernameEditableTextNode: AuthTextField
  private let passwordEditableTextNode: AuthTextField

  // MARK: - Lifecycle

  override init() {
    introPagerNode = ASPagerNode()
//    introPageControl = UIPageControl()

    signUpTermPrivacyTextNode = ASTextNode()

    authButton = AuthButton(title: AuthText.AuthViewController.loginButtonTitle,
                            gradiant: true,
                            backgroundColor: POColor.gray(1),
                            size: CGSize(width: POUI.screenWidth - POUI.size(60), height: POUI.size(50)))

    switchModeButton = ASButtonNode()
    resetPasswordButton = ASButtonNode()

    emailEditableTextNode = AuthTextField(placeholder: AuthText.AuthViewController.emailTextFieldPlaceholder, height: POUI.size(50))
    usernameEditableTextNode = AuthTextField(placeholder: AuthText.AuthViewController.usernameLoginTextFieldPlaceholder, height: POUI.size(50))
    passwordEditableTextNode = AuthTextField(placeholder: AuthText.AuthViewController.passwordLoginTextFieldPlaceholder, height: POUI.size(50))

    super.init()

    automaticallyManagesContentSize = true
    automaticallyManagesSubnodes = true

    subscribe()
  }

  deinit {
    unsubscribe()
  }

  override func didLoad() {
    super.didLoad()

    setupViews()
  }

  // MARK: - Layout

  private func setupViews() {
    backgroundColor = POColor.gray(1)

    introPagerNode.setDelegate(self)
    introPagerNode.setDataSource(self)
    introPagerNode.allowsAutomaticInsetsAdjustment = true
    introPagerNode.view.bounces = false

//    introPageControl.backgroundColor = .clear
//    introPageControl.numberOfPages = introPagerItem.count
//    introPageControl.currentPage = 0
//    introPageControl.pageIndicatorTintColor = POColor.gray(5)
//    introPageControl.currentPageIndicatorTintColor = POColor.yellow

    signUpTermPrivacyTextNode.linkAttributeNames = [NSAttributedStringKey.link.rawValue]
    let text = AuthText.AuthViewController.signUpTermPrivacyText
    let nsText = NSString(string: text)
    let mutableAttributeString = NSMutableAttributedString(string: text, attributes: [
      NSAttributedStringKey.foregroundColor: POColor.gray(4),
      NSAttributedStringKey.font: UIFont.regularTextFont(withSize: 12),
    ])
    mutableAttributeString.addAttributes([
      NSAttributedStringKey.font: UIFont.mediumTextFont(withSize: 12),
      NSAttributedStringKey.underlineColor: POColor.gray(4),
      NSAttributedStringKey.link: URL(string: POWebsiteURL.terms)!,
    ], range: nsText.range(of: AuthText.AuthViewController.signUpTermPrivacyTextTerms))
    mutableAttributeString.addAttributes([
      NSAttributedStringKey.font: UIFont.mediumTextFont(withSize: 12),
      NSAttributedStringKey.underlineColor: POColor.gray(4),
      NSAttributedStringKey.link: URL(string: POWebsiteURL.privacyPolicy)!,
    ], range: nsText.range(of: AuthText.AuthViewController.signUpTermPrivacyTextPrivacyPolicy))
    signUpTermPrivacyTextNode.attributedText = mutableAttributeString
    signUpTermPrivacyTextNode.isUserInteractionEnabled = true
    signUpTermPrivacyTextNode.isOpaque = true
    signUpTermPrivacyTextNode.backgroundColor = POColor.gray(1)
    signUpTermPrivacyTextNode.style.spacingAfter = POUI.size(4)

    [authButton].forEach {
      $0.style.preferredLayoutSize = ASLayoutSize(width: ASDimension(unit: .fraction, value: 1), height: ASDimension(unit: .points, value: POUI.size(50)))
    }

    switchModeButton.setTitle(AuthText.AuthViewController.switchModeLoginButtonTitle, with: UIFont.mediumTextFont(withSize: 14), with: POColor.black, for: .normal)
    switchModeButton.setTitle(AuthText.AuthViewController.switchModeLoginButtonTitle, with: UIFont.mediumTextFont(withSize: 14), with: POColor.gray(2), for: .highlighted)
    switchModeButton.titleNode.isOpaque = true
    switchModeButton.titleNode.backgroundColor = POColor.gray(1)
    switchModeButton.style.height = ASDimension(unit: .points, value: POUI.size(38))
    switchModeButton.addTarget(self, action: #selector(didTapSwitchModeButton), forControlEvents: .touchUpInside)

    resetPasswordButton.setTitle(AuthText.AuthViewController.forgotPasswordButtonTitle, with: UIFont.mediumTextFont(withSize: 14), with: POColor.black, for: .normal)
    resetPasswordButton.setTitle(AuthText.AuthViewController.forgotPasswordButtonTitle, with: UIFont.mediumTextFont(withSize: 14), with: POColor.gray(2), for: .highlighted)
    resetPasswordButton.titleNode.isOpaque = true
    resetPasswordButton.titleNode.backgroundColor = POColor.gray(1)
    resetPasswordButton.style.height = ASDimension(unit: .points, value: POUI.size(38))
    resetPasswordButton.addTarget(self, action: #selector(didTapForgotPasswordButton), forControlEvents: .touchUpInside)

    [emailEditableTextNode, usernameEditableTextNode, passwordEditableTextNode].forEach {
      $0.isSecureTextEntry = true
    }

    authButton.addTarget(self, action: #selector(didTapAuthButton), forControlEvents: .touchUpInside)

    view.delegate = self
    view.showsVerticalScrollIndicator = false

    emailEditableTextNode.delegate = self
    usernameEditableTextNode.delegate = self
    passwordEditableTextNode.delegate = self

    signUpTermPrivacyTextNode.delegate = self

    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapScrollView)))

    authButton.accessibilityIdentifier = "authButton"
    emailEditableTextNode.accessibilityIdentifier = "emailEditableTextNode"
    usernameEditableTextNode.accessibilityIdentifier = "usernameEditableTextNode"
    passwordEditableTextNode.accessibilityIdentifier = "passwordEditableTextNode"
    switchModeButton.accessibilityIdentifier = "switchModeButton"
    resetPasswordButton.accessibilityIdentifier = "resetPasswordButton"
  }

  override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
    introPagerNode.style.preferredSize = POUI.size(width: 414, height: 574)
//    introPageControlNode.style.preferredSize = POUI.size(width: 40, height: 8)
//    let introControlNodeLayout = ASRelativeLayoutSpec(horizontalPosition: .center, verticalPosition: .end, sizingOption: [], child: introPageControlNode)
//    let introControlNodeInsetLayout = ASInsetLayoutSpec(insets: UIEdgeInsetsMake(0, 0, 16, 0), child: introControlNodeLayout)

    let switchModeBarLayoutSpec: ASStackLayoutSpec!
    let buttonStackLayoutSpec: ASStackLayoutSpec!

    switch state {
    case let .login(expanded):
      switchModeBarLayoutSpec = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: .spaceBetween, alignItems: .center, children: [switchModeButton, resetPasswordButton])
      switch expanded {
      case true:
        buttonStackLayoutSpec = ASStackLayoutSpec(direction: .vertical, spacing: POUI.size(12), justifyContent: .spaceBetween, alignItems: .center, children: [
          signUpTermPrivacyTextNode,
          usernameEditableTextNode,
          passwordEditableTextNode,
          authButton,
          switchModeBarLayoutSpec,
        ])
      case false:
        buttonStackLayoutSpec = ASStackLayoutSpec(direction: .vertical, spacing: POUI.size(12), justifyContent: .spaceBetween, alignItems: .center, children: [
          signUpTermPrivacyTextNode,
          authButton,
          switchModeBarLayoutSpec,
        ])
      }
    case let .signUp(expanded):
      switchModeBarLayoutSpec = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: .center, alignItems: .center, children: [switchModeButton])
      switch expanded {
      case true:
        buttonStackLayoutSpec = ASStackLayoutSpec(direction: .vertical, spacing: POUI.size(12), justifyContent: .spaceBetween, alignItems: .center, children: [
          signUpTermPrivacyTextNode,
          emailEditableTextNode,
          usernameEditableTextNode,
          passwordEditableTextNode,
          authButton,
          switchModeBarLayoutSpec,
        ])
      case false:
        buttonStackLayoutSpec = ASStackLayoutSpec(direction: .vertical, spacing: POUI.size(12), justifyContent: .spaceBetween, alignItems: .center, children: [
          signUpTermPrivacyTextNode,
          authButton,
          switchModeBarLayoutSpec,
        ])
      }
    }

    switchModeBarLayoutSpec.style.width = ASDimension(unit: .fraction, value: 1)
    switchModeBarLayoutSpec.style.spacingBefore = POUI.size(6)
    switchModeBarLayoutSpec.style.spacingAfter = POUI.size(10)

    buttonStackLayoutSpec.style.width = ASDimension(unit: .fraction, value: 1)

    let buttonInsetLayoutSpec = ASInsetLayoutSpec(insets: UIEdgeInsetsMake(0, POUI.size(30), 0, POUI.size(30)), child: buttonStackLayoutSpec)

    let justifyContent = UIDevice.current.isX ? ASStackLayoutJustifyContent.spaceBetween : ASStackLayoutJustifyContent.center

    let stackLayoutSpec = ASStackLayoutSpec(direction: .vertical, spacing: POUI.size(20), justifyContent: justifyContent, alignItems: .center, children: [
//      ASOverlayLayoutSpec(child: introPagerNode, overlay: introControlNodeInsetLayout),
      introPagerNode,
      buttonInsetLayoutSpec,
    ])

    return stackLayoutSpec
  }

  // MARK: - UI Interaction

  override func didCompleteLayoutTransition(_ context: ASContextTransitioning) {
    super.didCompleteLayoutTransition(context)

    var expanded = false

    switch state {
    case let .login(isExpanded):
      expanded = isExpanded
    case let .signUp(isExpanded):
      expanded = isExpanded
    }

    if expanded {
      let bottomOffset = CGPoint(x: 0, y: view.contentSize.height - view.bounds.size.height)
      view.setContentOffset(bottomOffset, animated: true)
    }
  }

  // MARK: - User Interaction

  @objc private func didTapScrollView() {
    view.endEditing(true)
  }

  @objc private func didTapForgotPasswordButton() {
    delegate?.didTapForgotPasswordButton()
  }

  @objc private func didTapSwitchModeButton() {
    switch state {
    case .login:
      state = .signUp(isExpanded: true)
    case .signUp:
      state = .login(isExpanded: true)
    }
  }

  @objc private func didTapAuthButton() {
    switch state {
    case let .login(isExpanded):
      switch isExpanded {
      case false:
        state = .login(isExpanded: true)
      case true:
        didTapLoginButton(username: username, password: password)
      }
    case let .signUp(isExpanded):
      switch isExpanded {
      case false:
        state = .signUp(isExpanded: true)
      case true:
        didTapSignUpButton(email: email, username: username, password: password)
      }
    }
  }

  private func didTapLoginButton(username: Username?, password: Password?) {
    guard let username = username else {
      usernameEditableTextNode.setStatus(.danger)
      usernameEditableTextNode.becomeFirstResponder()
      return
    }

    guard let password = password, password.count > 0 else {
      passwordEditableTextNode.setStatus(.danger)
      passwordEditableTextNode.becomeFirstResponder()
      return
    }

    delegate?.didTapLoginButton(username: username, password: password)
  }

  private func didTapSignUpButton(email: Email?, username: Username?, password: Password?) {
    guard let email = email else {
      emailEditableTextNode.setStatus(.danger)
      emailEditableTextNode.becomeFirstResponder()
      return
    }

    guard let username = username else {
      usernameEditableTextNode.setStatus(.danger)
      usernameEditableTextNode.becomeFirstResponder()
      return
    }

    guard let password = password, password.count > 0 else {
      passwordEditableTextNode.setStatus(.danger)
      passwordEditableTextNode.becomeFirstResponder()
      return
    }

    guard email.isValidEmail() else {
      emailEditableTextNode.setStatus(.danger)
      delegate?.showAlert(AuthText.Error.emailInvalidFormat)
      emailEditableTextNode.becomeFirstResponder()
      return
    }

    guard username.count >= poauthUsernameMinimumCharacters,
      username.count <= poauthUsernameMaximumCharacters else {
      usernameEditableTextNode.setStatus(.danger)
      delegate?.showAlert(AuthText.Error.usernameLengthIncorrect)
      usernameEditableTextNode.becomeFirstResponder()
      return
    }

    guard password.count >= poauthPasswordMinimumCharacters else {
      passwordEditableTextNode.setStatus(.danger)
      delegate?.showAlert(AuthText.Error.passwordLengthIncorrect)
      passwordEditableTextNode.becomeFirstResponder()
      return
    }

    delegate?.didTapSignUpButton(email: email, username: username, password: password)
  }

  // MARK: - Notification

  func subscribe() {
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }

  func unsubscribe() {
    NotificationCenter.default.removeObserver(self)
  }

  @objc private func keyboardWillShow(notification: NSNotification) {
    if let userInfo = notification.userInfo,
      let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height,
      let isCurrentApp = userInfo[UIKeyboardIsLocalUserInfoKey] as? Bool, isCurrentApp {
      view.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight + 20, right: 0)
      view.scrollRectToVisible(switchModeButton.frame, animated: true)
    }
  }

  @objc private func keyboardWillHide(notification _: NSNotification) {
    UIView.animate(withDuration: 0.2, animations: {
      self.view.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    })
  }
}

extension AuthScrollNode: ASPagerDataSource, ASPagerDelegate {
  func numberOfPages(in _: ASPagerNode) -> Int {
    return introPagerItem.count
  }

  func pagerNode(_: ASPagerNode, nodeAt index: Int) -> ASCellNode {
    return AuthIntroPage(introItem: introPagerItem[index])
  }

  func scrollViewDidEndDecelerating(_: UIScrollView) {
//    introPageControl.currentPage = introPagerNode.currentPageIndex
  }
}

extension AuthScrollNode: UIScrollViewDelegate {
  func scrollViewWillBeginDragging(_: UIScrollView) {
    view.endEditing(true)
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if UIDevice.current.isX {
      let bottomOffset = CGPoint(x: 0, y: view.contentSize.height - view.bounds.size.height)
      if scrollView.contentOffset.y < bottomOffset.y {
        scrollView.setContentOffset(bottomOffset, animated: false)
      }
    }
  }
}

extension AuthScrollNode: ASEditableTextNodeDelegate {
  func editableTextNodeDidUpdateText(_ editableTextNode: ASEditableTextNode) {
    guard let textNode = editableTextNode as? AuthTextField else {
      return
    }

    textNode.setStatus(.clear)

    switch textNode {
    case passwordEditableTextNode:
      var securedText = ""

      for _ in 0 ..< password.count {
        securedText.append("•")
      }

      textNode.setText(securedText as String)
    default:
      break
    }
  }

  func editableTextNode(_ editableTextNode: ASEditableTextNode, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    // handle return key
    if text == "\n" {
      switch editableTextNode {
      case emailEditableTextNode:
        emailEditableTextNode.resignFirstResponder()
        usernameEditableTextNode.becomeFirstResponder()
      case usernameEditableTextNode:
        usernameEditableTextNode.resignFirstResponder()
        passwordEditableTextNode.becomeFirstResponder()
      case passwordEditableTextNode:
        passwordEditableTextNode.resignFirstResponder()
        didTapAuthButton()
      default:
        break
      }
      return false
    }

    // handle password mask
    if editableTextNode == passwordEditableTextNode {
      if text.lengthOfBytes(using: .utf8) == 0 {
        (0 ..< range.length).forEach { _ in
          if password.count != 0 {
            password.removeLast()
          }
        }
      } else {
        password += text
      }
    }

    return true
  }
}

extension AuthScrollNode: ASTextNodeDelegate {
  func textNode(_: ASTextNode, tappedLinkAttribute _: String, value: Any, at _: CGPoint, textRange _: NSRange) {
    guard let URL = value as? URL else { return }

    UIApplication.shared.open(URL, options: [:], completionHandler: nil)
  }
}
