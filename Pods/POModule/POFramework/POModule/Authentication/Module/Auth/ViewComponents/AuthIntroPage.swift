//
//  AuthIntroPage.swift
//  PinOn
//
//  Created by Kefei Qian on 11/16/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import POHelper

final class AuthIntroPage: ASCellNode {

  // MARK: - Variables

  // MARK: - View Components

  private let introImageNode: ASImageNode = {
    let introImageNode = ASImageNode()
    introImageNode.isOpaque = true
    introImageNode.backgroundColor = POColor.gray(1)

    return introImageNode
  }()

  // MARK: - Lifecycle

  required init(introItem: UIImage) {
    super.init()

    automaticallyManagesSubnodes = true
    introImageNode.image = introItem
  }

  // MARK: - Layout

  override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
    return ASWrapperLayoutSpec(layoutElement: introImageNode)
  }
}
