//
//  AuthInteractor.swift
//  PinOn
//
//  Created by Kefei Qian on 11/16/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import CocoaLumberjack
import POAuth
import RxSwift
import SwiftyJSON

final class AuthInteractor {

  // MARK: - Properties

  weak var output: AuthInteractorOutput?

  private let disposeBag = DisposeBag()
}

extension AuthInteractor: AuthUseCase {
  func emailLogin(with username: Username, password: Password) {
    POAuth.auth.login(with: username, password: password)
      .asObservable()
      .subscribe { event in
        switch event {
        case .next:
          break
        case .completed:
          self.output?.loginSuccessful()
        case let .error(error):
          self.output?.authFailed(error: error)
        }
      }.disposed(by: disposeBag)
  }

  func emailSignUp(with email: Email, username: Username, password: Password) {
    POAuth.auth.signUp(with: email, username: username, password: password)
      .asObservable()
      .subscribe { event in
        switch event {
        case .next:
          break
        case .completed:
          self.output?.signUpSuccessful()
        case let .error(error):
          self.output?.authFailed(error: error)
        }
      }.disposed(by: disposeBag)
  }
}
