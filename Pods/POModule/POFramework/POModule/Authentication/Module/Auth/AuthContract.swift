//
//  AuthContract.swift
//  PinOn
//
//  Created by Kefei Qian on 11/15/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit

typealias Email = String
typealias Password = String
typealias Username = String
typealias Message = String

enum AuthState {
  case login(isExpanded: Bool)
  case signUp(isExpanded: Bool)
}

protocol AuthView: class {
  var presenter: AuthPresentation! { get set }
  var uiState: AuthUIState { get set }
}

protocol AuthUseCase { // Interactor
  weak var output: AuthInteractorOutput? { get set }
  func emailLogin(with username: Username, password: Password)
  func emailSignUp(with email: Email, username: Username, password: Password)
}

protocol AuthPresentation { // Presenter
  weak var view: AuthView? { get set }
  var interactor: AuthUseCase! { get set }
  var router: AuthWireframe! { get set }

  func didTapLoginButton(with username: Username, password: Password)
  func didTapSignUpButton(with email: Email, username: Username, password: Password)
  func didTapForgotPasswordButton()
}

protocol AuthInteractorOutput: class { // Presenter
  func loginSuccessful()
  func signUpSuccessful()
  func authFailed(error: Error)
}

protocol AuthWireframe: class { // Router
  func runResetPasswordFlow()
  func authDidFinishLogin()
  func authDidFinishSignUp()
}
