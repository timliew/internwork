//
//  AuthViewController.swift
//  PinOn
//
//  Created by Kefei Qian on 11/15/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack
import SwiftyJSON

enum AuthUIState {
  case loading
  case success
  case failure(message: Message?, showTime: TimeInterval)
}

final class AuthViewController: ASViewController<ASScrollNode>, AuthView {

  // MARK: - Properties

  var presenter: AuthPresentation!

  // MARK: - Variables

  var uiState: AuthUIState = .success {
    didSet {
      switch uiState {
      case .loading:
        startAuthActivity()
      case let .failure(message, showTime):
        endAuthActivity()
        showAuthMessage(message, time: showTime)
      case .success:
        endAuthActivity()
      }
    }
  }

  // MARK: - View Components

  private var authViewNode: ASScrollNode & AuthScrollNodeView = AuthScrollNode()

  // MARK: - Lifecycle

  required init() {
    DDLogVerbose("init")
    super.init(node: authViewNode)
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  deinit {
    DDLogVerbose("deinit")
  }

  // MARK: - View Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()

    authViewNode.delegate = self
  }

  // MARK: - Layout

  override var prefersStatusBarHidden: Bool {
    return true
  }

  // MARK: - UI Interaction

  // MARK: - User Interaction

  // MARK: - Controller Logic

  // MARK: - Notifications

  // MARK: - Helpers

}

extension AuthViewController: AuthScrollNodeDelegate {
  func showAlert(_ message: Message) {
    showAuthMessage(message)
  }

  func didTapLoginButton(username: Username, password: Password) {
    presenter.didTapLoginButton(with: username, password: password)
  }

  func didTapSignUpButton(email: Email, username: Username, password: Password) {
    presenter.didTapSignUpButton(with: email, username: username, password: password)
  }

  func didTapForgotPasswordButton() {
    presenter.didTapForgotPasswordButton()
  }
}
