//
//  AuthPresenter.swift
//  PinOn
//
//  Created by Kefei Qian on 11/16/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import CocoaLumberjack
import POAuth

final class AuthPresenter {

  // MARK: - Properties

  weak var view: AuthView?
  var router: AuthWireframe!
  var interactor: AuthUseCase!
}

extension AuthPresenter: AuthPresentation {
  func didTapForgotPasswordButton() {
    router.runResetPasswordFlow()
  }

  func didTapLoginButton(with username: Username, password: Password) {
    view?.uiState = .loading
    interactor.emailLogin(with: username, password: password)
  }

  func didTapSignUpButton(with email: Email, username: Username, password: Password) {
    view?.uiState = .loading
    interactor.emailSignUp(with: email, username: username, password: password)
  }
}

extension AuthPresenter: AuthInteractorOutput {
  func loginSuccessful() {
    view?.uiState = .success
    router.authDidFinishLogin()
  }

  func signUpSuccessful() {
    view?.uiState = .success
    router.authDidFinishSignUp()
  }

  func authFailed(error: Error) {
    switch error {
    case POAuthError.usernamePasswordDoNotMatch:
      view?.uiState = .failure(message: AuthText.Error.usernamePasswordDoNotMatch, showTime: 2)
    case POAuthError.usernameUnavailable:
      view?.uiState = .failure(message: AuthText.Error.usernameUnavailable, showTime: 2)
    case POAuthError.usernameInvalidFormat:
      view?.uiState = .failure(message: AuthText.Error.usernameInvalidFormat, showTime: 2)
    case POAuthError.emailUnavailable:
      view?.uiState = .failure(message: AuthText.Error.emailUnavailable, showTime: 2)
    case POAuthError.emailInvalidFormat:
      view?.uiState = .failure(message: AuthText.Error.emailInvalidFormat, showTime: 2)
    default:
      view?.uiState = .failure(message: error.localizedDescription, showTime: 2)
    }
  }
}
