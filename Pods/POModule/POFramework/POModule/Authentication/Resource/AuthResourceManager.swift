//
//  AuthResourceController.swift
//  POFramework
//
//  Created by Kefei Qian on 2/28/18.
//  COPYRIGHT © 2018-PRESENT CocoaPods ALL RIGHTS RESERVED.
//

import POHelper
import UIKit

public final class AuthResourceManager: POResourceManager {
  public override class func resourceBundle() -> Bundle {
    let assetPath = Bundle(for: AuthResourceManager.self).resourcePath!
    return Bundle(path: (assetPath as NSString).appendingPathComponent("AuthResource.bundle"))!
  }

  // MARK: - Images

  public class func authIntroPagerNodeImage1() -> UIImage {
    return imageForResource("AuthIntroPagerNodeImage1")
  }

  public class func authIntroPagerNodeImage2() -> UIImage {
    return imageForResource("AuthIntroPagerNodeImage2")
  }

  public class func authIntroPagerNodeImage3() -> UIImage {
    return imageForResource("AuthIntroPagerNodeImage3")
  }

  public class func authResetPasswordBackground() -> UIImage {
    return imageForResource("AuthResetPasswordBackground")
  }

  public class func authResetPasswordBackButton() -> UIImage {
    return imageForResource("AuthResetPasswordBackButton")
  }

  public class func authButtonBackground() -> UIImage {
    return imageForResource("AuthButtonBackground")
  }

  public class func authButtonHighlightedBackground() -> UIImage {
    return imageForResource("AuthButtonHighlightedBackground")
  }
}
