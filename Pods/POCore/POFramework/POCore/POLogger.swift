//
//  POLogger.swift
//  POCore
//
//  Created by Kefei Qian on 2/8/18.
//  COPYRIGHT © 2018-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import CocoaLumberjack
import Foundation

public final class POLogger {
  public static let sharedInstance = POLogger()

  public static func setup() {
    DDTTYLogger.sharedInstance.logFormatter = POLoggerFormatter()
    DDLog.add(DDTTYLogger.sharedInstance)

    let fileLogger: DDFileLogger = DDFileLogger() // File Logger
    fileLogger.rollingFrequency = TimeInterval(60 * 60 * 24) // 24 hours
    fileLogger.logFileManager.maximumNumberOfLogFiles = 365
    DDLog.add(fileLogger)
  }
}

final class POLoggerFormatter: NSObject, DDLogFormatter {
  private var dateFormatter: DateFormatter = {
    let df = DateFormatter()
    df.formatterBehavior = .behavior10_4
    df.dateFormat = "HH:mm:ss.SSS"
    return df
  }()

  func format(message logMessage: DDLogMessage) -> String? {
    let dateAndTime = dateFormatter.string(from: logMessage.timestamp)

    var logLevel: String
    var logIcon: String
    var useLog = true
    let logFlag: DDLogFlag = logMessage.flag

    if logFlag.contains(.verbose) {
      logLevel = "VERBOSE"
      logIcon = "😘"
    } else if logFlag.contains(.debug) {
      logLevel = "DEBUG"
      logIcon = "🤔"
    } else if logFlag.contains(.info) {
      logLevel = "INFO"
      logIcon = "ℹ️️"
    } else if logFlag.contains(.warning) {
      logLevel = "WARNING"
      logIcon = "⚠️️"
    } else if logFlag.contains(.error) {
      logLevel = "ERROR"
      logIcon = "🚫"
    } else {
      logLevel = "OFF"
      logIcon = ""
      useLog = false
    }

    var formattedLog: String?

    if useLog {
      formattedLog = "\(dateAndTime) \(logIcon) \(logLevel) [\(logMessage.fileName).\(logMessage.function!)] #\(logMessage.line): \(logMessage.message)"
    }

    return formattedLog
  }
}
