import POHelper

public typealias GraphQLEndpoint = URL
public typealias HTTPHeaders = [String: String]
public typealias AppName = String

public final class POApp {
  public static let app = POApp()

  public private(set) var appName: AppName!
  public private(set) var graphQLEndpoint: GraphQLEndpoint!

  public var systemInfo: [String: String] {
    enum SystemInfoKeyname: String {
      case appVersion = "app_version"
      case systemVersion = "system_version"
      case systemName = "system_name"
      case mobileName = "mobile_name"
      case clientModel = "client_model"
      case udid
      case batteryLevel = "battery_level"
      case batteryState = "battery_state"
      case language
    }

    return [
      SystemInfoKeyname.appVersion.rawValue: Bundle.main.releaseVersionNumber ?? "",
      SystemInfoKeyname.systemVersion.rawValue: UIDevice.current.systemVersion,
      SystemInfoKeyname.systemName.rawValue: UIDevice.current.systemName,
      SystemInfoKeyname.mobileName.rawValue: UIDevice.current.name,
      SystemInfoKeyname.clientModel.rawValue: UIDevice.current.model,
      SystemInfoKeyname.udid.rawValue: UIDevice.current.identifierForVendor?.uuidString ?? "",
      SystemInfoKeyname.batteryLevel.rawValue: String(UIDevice.current.batteryLevel),
      SystemInfoKeyname.batteryState.rawValue: String(UIDevice.current.batteryState.rawValue),
      SystemInfoKeyname.language.rawValue: Locale.preferredLanguages[0],
    ]
  }

  private init() {}

  public static func setup(with appName: AppName, graphQLEndpoint: GraphQLEndpoint) {
    POLogger.setup()
    app.appName = appName
    app.graphQLEndpoint = graphQLEndpoint
//    POAuth.setup(appName: appName)
//
//    let headers: HTTPHeaders?
//    if let currentUser = POAuth.sharedInstance.currentUser {
//      headers = [
//        PONetworkManagerHeaderKeyName.userId.rawValue: currentUser.userId,
//        PONetworkManagerHeaderKeyName.idToken.rawValue: currentUser.accessToken,
//      ]
//    } else {
//      headers = nil
//    }
//
//    PONetworkManager.setup(with: graphQLEndpoint, headers: headers)
  }
}
