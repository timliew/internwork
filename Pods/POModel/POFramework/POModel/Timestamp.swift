//
//  Timestamp.swift
//  PinOn
//
//  Created by Kefei Qian on 12/10/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import CocoaLumberjack
import SwiftyJSON

enum TimestampKeyName: String, Codable {
  case timeRough
  case timeDetail
  case timeTitle
  case timeAccurate
  case time
}

public struct Timestamp: Codable {
  public let time: TimeInterval
  public let text: String

  public init?(timeInfo: JSON) {
    guard let time = timeInfo[TimestampKeyName.time.rawValue].double else {
      DDLogError("Cannot parse timeInfo: \(timeInfo)")
      return nil
    }

    self.time = time
    let timeRough = timeInfo[TimestampKeyName.timeRough.rawValue].string
    let timeDetail = timeInfo[TimestampKeyName.timeDetail.rawValue].string
    let timeTitle = timeInfo[TimestampKeyName.timeTitle.rawValue].string
    let timeAccurate = timeInfo[TimestampKeyName.timeAccurate.rawValue].string

    guard timeRough != nil
      || timeDetail != nil
      || timeTitle != nil
      || timeAccurate != nil else {
      DDLogError("Cannot parse timeInfo: \(timeInfo)")
      return nil
    }

    text = [
      timeRough,
      timeDetail,
      timeTitle,
      timeAccurate,
    ]
    .flatMap { $0 }
    .joined(separator: "")
  }

  public init() {
    time = Date().timeIntervalSince1970
    text = "Just Now"
  }
}
