/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 8/20/17.
 */

import CocoaLumberjack
import SwiftyJSON

enum UserKeyName: String {
  case userId
  case username
  case avatarUrl
  case relations
  case isPioneer
  case pin
  case pinAmount
  case createTime
  case isFeatured
  case refreshToken
  case accessToken
  case bio
}

public struct User: Codable, Equatable, CustomStringConvertible {
  public let userId: String
  public let avatarUrl: String
  public let username: String
  public let createTime: Timestamp?

  public let bio: String?

  public let pinAmount: Int

  private(set) var relations: Relations

  public private(set) var accessToken: String?
  public private(set) var refreshToken: String?

  private(set) var isPioneer: Bool
  private(set) var isFeatured: Bool

  public init?(userInfo: JSON, refreshToken: String? = nil, accessToken: String? = nil) {
    guard let userId = userInfo[UserKeyName.userId.rawValue].string,
      let username = userInfo[UserKeyName.username.rawValue].string,
      let avatarUrl = userInfo[UserKeyName.avatarUrl.rawValue].string,
      let relations = Relations(relationsInfo: userInfo[UserKeyName.relations.rawValue])
    else {
      DDLogError("Cannot parse user data: \(userInfo)")
      return nil
    }

    self.relations = relations
    self.userId = userId
    self.username = username
    self.avatarUrl = avatarUrl

    bio = userInfo[UserKeyName.bio.rawValue].string

    isPioneer = userInfo[UserKeyName.isPioneer.rawValue].bool ?? false
    isFeatured = userInfo[UserKeyName.isFeatured.rawValue].bool ?? false

    createTime = Timestamp(timeInfo: userInfo[UserKeyName.createTime.rawValue])

    self.refreshToken = refreshToken
    self.accessToken = accessToken

    pinAmount = userInfo[UserKeyName.pin.rawValue][UserKeyName.pinAmount.rawValue].int ?? 0
  }

  public var description: String {
    return """
    userID: \(userId)
    username: \(username)
    avatarUrl: \(avatarUrl)
    \(relations)
    createTime:\(String(describing: createTime))
    bio: \(String(describing: bio))
    pinAmount: \(String(describing: pinAmount))
    accessToken: \(String(describing: accessToken))
    refreshToken:\(String(describing: refreshToken))
    """ as String
  }

  public static func == (lhs: User, rhs: User) -> Bool {
    return lhs.userId == rhs.userId
  }

//  public override func isEqual(_ object: Any?) -> Bool {
//    return userId == (object as? User)?.userId
//  }
//
//  public func updateRelations(_ relations: Relations) {
//    self.relations = relations
//  }
//
//  public func updatePioneerBadge(isPioneer: Bool = true) {
//    self.isPioneer = isPioneer
//  }

//  public init?(searchUserInfo: JSON) {
//    guard let userId = searchUserInfo[UserKeyName.userId.rawValue].string,
//      let username = searchUserInfo[UserKeyName.username.rawValue].string,
//      let avatarUrl = searchUserInfo[UserKeyName.avatarUrl.rawValue].string else {
//      return nil
//    }
//
//    self.userId = userId
//    self.username = username
//    self.avatarUrl = avatarUrl
//    createTime = nil
//    isPioneer = false
//    isFeatured = false
//    relations = Relations()
//    pinAmount = 0
//    bio = nil
//  }
}

// MARK: - Delegate
