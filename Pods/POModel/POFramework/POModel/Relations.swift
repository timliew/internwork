//
//  Relations.swift
//  PinOn
//
//  Created by Kefei Qian on 12/14/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//

import Foundation
import SwiftyJSON

enum RelationsKeyName: String {
  case type
  case followerAmount
  case followingAmount
}

public enum Relation: String, Codable {
  case myself
  case block
  case stranger
  case following
  case friend
}

public struct Relations: CustomStringConvertible, Codable {
  public let relation: Relation
  public let followingAmount: Int
  public let followerAmount: Int

  public init?(relationsInfo: JSON) {
    guard let relationType = relationsInfo[RelationsKeyName.type.rawValue].string,
      let relation = Relation(rawValue: relationType) else {
      return nil
    }

    self.relation = relation
    followerAmount = relationsInfo[RelationsKeyName.followerAmount.rawValue].int ?? 0
    followingAmount = relationsInfo[RelationsKeyName.followingAmount.rawValue].int ?? 0
  }

  public var description: String {
    return """
    relation: \(relation)
    followingAmount: \(String(describing: followingAmount))
    followerAmount: \(String(describing: followerAmount))
    """
  }

  public init() {
    relation = Relation.myself
    followerAmount = 0
    followingAmount = 0
  }

  public init(relation: Relation, followerAmount: Int = 0, followingAmount: Int = 0) {
    self.relation = relation
    self.followerAmount = followerAmount
    self.followingAmount = followingAmount
  }
}
