//
//  CoordinatorContract.swift
//  PinOn
//
//  Created by Kefei Qian on 11/13/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//
import CocoaLumberjack

open class POCoordinator {
  public private(set) var name: String

  private let lock = DispatchSemaphore(value: 1)

  private var parentCoordinator: POCoordinator?

  private var childCoordinators: [POCoordinator] = [] {
    didSet {
      DDLogDebug("\(name) childCoordinators: \(childCoordinators.description)")
    }
  }

  public init(name: String) {
    self.name = name
  }

  open func start() {
    fatalError("Coordinator must implement this method")
  }

  // add only unique object
  public func addDependency(_ coordinator: POCoordinator) {
    lock.wait(); defer { lock.signal() }
    for element in childCoordinators {
      if element.name == coordinator.name {
        fatalError("already added \(coordinator.name)")
      }
    }
    coordinator.parentCoordinator = self
    childCoordinators.append(coordinator)
  }

  public func removeAllDependency() {
    for (_, element) in childCoordinators.enumerated() {
      element.removeAllDependency()
    }
    childCoordinators.removeAll()
  }

  public func removeFromParentCoordinator() {
    childCoordinators.forEach { coordinator in
      coordinator.removeFromParentCoordinator()
    }
    parentCoordinator?.removeDependency(self)
  }

  public func removeDependency(_ coordinator: POCoordinator?) {
    guard childCoordinators.isEmpty == false,
      let coordinator = coordinator else {
      return
    }
    lock.wait(); defer { lock.signal() }
    for (index, element) in childCoordinators.enumerated() {
      if element.name == coordinator.name {
        childCoordinators.remove(at: index)
      }
    }
  }
}
