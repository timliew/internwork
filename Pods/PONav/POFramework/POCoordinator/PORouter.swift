//
//  Router.swift
//  PinOn
//
//  Created by Kefei Qian on 11/13/17.
//  COPYRIGHT © 2017-PRESENT PinOn, Inc. ALL RIGHTS RESERVED.
//
import UIKit

public protocol Presentable {
  func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
  public func toPresent() -> UIViewController? {
    return self
  }
}

protocol PORouterProtocol: Presentable {
  func present(_ module: Presentable?)
  func present(_ module: Presentable?, animated: Bool)

  func push(_ module: Presentable?)
  func push(_ module: Presentable?, animated: Bool)
  func push(_ module: Presentable?, animated: Bool, completion: (() -> Void)?)

  func popModule()
  func popModule(animated: Bool)

  func dismissModule()
  func dismissModule(animated: Bool, completion: (() -> Void)?)

  func setRootModule(_ module: Presentable?)
  func setRootModule(_ module: Presentable?, hideBar: Bool, animated: Bool)

  func popToRootModule(animated: Bool)
}

public final class PORouter: NSObject, PORouterProtocol {
  public private(set) weak var rootController: UINavigationController?

  public init(rootController: UINavigationController) {
    self.rootController = rootController
  }

  public func toPresent() -> UIViewController? {
    return rootController
  }

  public func printChildViewControllers() {
    print(rootController?.viewControllers as Any)
  }

  public func present(_ module: Presentable?) {
    present(module, animated: true)
  }

  public func present(_ module: Presentable?, animated: Bool) {
    guard let controller = module?.toPresent() else { return }
    DispatchQueue.main.async {
      self.rootController?.present(controller, animated: animated, completion: nil)
    }
  }

  public func dismissModule() {
    dismissModule(animated: true, completion: nil)
  }

  public func dismissModule(animated: Bool, completion: (() -> Void)?) {
    rootController?.dismiss(animated: animated, completion: completion)
  }

  public func push(_ module: Presentable?) {
    push(module, animated: true)
  }

  public func push(_ module: Presentable?, animated: Bool) {
    push(module, animated: animated, completion: nil)
  }

  public func push(_ module: Presentable?, animated: Bool, completion: (() -> Void)?) {
    guard
      let controller = module?.toPresent(),
      (controller is UINavigationController == false)
    else { assertionFailure("Deprecated push UINavigationController."); return }

    DispatchQueue.main.async {
      self.rootController?.pushViewController(controller, animated: animated)
    }
  }

  public func popModule() {
    popModule(animated: true)
  }

  public func popModule(animated: Bool) {
    rootController?.popViewController(animated: animated)
  }

  public func setRootModule(_ module: Presentable?) {
    setRootModule(module, hideBar: true)
  }

  public func setRootModule(_ module: Presentable?, hideBar: Bool, animated: Bool = false) {
    guard let controller = module?.toPresent() else { return }
    DispatchQueue.main.async {
      self.rootController?.setViewControllers([controller], animated: animated)
      self.rootController?.isNavigationBarHidden = hideBar
    }
  }

  public func popToRootModule(animated: Bool) {
    rootController?.popToRootViewController(animated: animated)
  }
}
