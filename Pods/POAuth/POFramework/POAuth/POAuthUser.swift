/**
 *   Copyright © 2017 PinOn, Inc. All rights reserved.
 *
 *   Created by Kefei Qian on 8/20/17.
 */

import CocoaLumberjack
import RxSwift
import SwiftyJSON

enum POAuthUserKeyName: String {
  case userId
  case username
  case avatarUrl
  case bio
  case refreshToken
  case accessToken
}

protocol POAuthUserInfo {
  var userId: UserId { get }
  var avatarUrl: AvatarUrl { get }
  var username: Username { get }

  var accessToken: AccessToken { get set }
}

public class POAuthUser: NSObject, Codable, POAuthUserInfo {
  public let userId: UserId
  public private(set) var avatarUrl: AvatarUrl
  public private(set) var username: Username

  public private(set) var bio: Bio?

  public var accessToken: AccessToken {
    get {
      return token.accessToken
    }
    set {
      token.accessToken = newValue
    }
  }

  let token: POAuthTokenService

  init?(userInfo: JSON, refreshToken: String, accessToken: String) {
    guard let userId = userInfo[POAuthUserKeyName.userId.rawValue].string,
      let username = userInfo[POAuthUserKeyName.username.rawValue].string,
      let avatarUrl = userInfo[POAuthUserKeyName.avatarUrl.rawValue].string
    else {
      DDLogError("Cannot parse user data: \(userInfo)")
      return nil
    }

    self.userId = userId
    self.username = username
    self.avatarUrl = avatarUrl
    bio = userInfo[POAuthUserKeyName.bio.rawValue].string

    token = POAuthTokenService(refreshToken: refreshToken, accessToken: accessToken, userId: userId)
  }

  public override var description: String {
    return """
    userID: \(userId)
    username: \(username)
    avatarUrl: \(avatarUrl)
    bio : \(bio ?? "")
    accessToken: \(token.accessToken)
    refreshToken: \(token.refreshToken)
    expireDate: \(token.accessTokenExpirationDate.timeIntervalSinceNow)
    """ as String
  }

  public static func == (lhs: POAuthUser, rhs: POAuthUser) -> Bool {
    return lhs.userId == rhs.userId
  }

  public func refreshAccessToken() -> Observable<Bool> {
    return token.refreshAccessToken(force: true)
  }

  public func update(username: Username?, avatarUrl: AvatarUrl?, bio: Bio?) {
    guard username != nil || avatarUrl != nil || bio != nil else {
      return
    }
    DispatchQueue.global().sync {
      if let username = username {
        self.username = username
      }
      if let avatarUrl = avatarUrl {
        self.avatarUrl = avatarUrl
      }
      if let bio = bio {
        self.bio = bio
      }
      NotificationCenter.default.post(name: POCurrentUserInfoDidChangeNotificationName, object: nil)
    }
  }
}

// MARK: - Delegate
