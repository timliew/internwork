import Foundation

public enum POAuthError: Int, Error {
  case rsaEncryptedError
  case userTokenDoesNotExist
  case unknownError
  case invalidServerResponse

  case usernameUnavailable = 50001
  case usernameInvalidFormat = 50002
  case emailUnavailable = 50003
  case emailInvalidFormat = 50004
  case usernamePasswordDoNotMatch = 50005
  case googleUserDoNotExist = 50006
  case facebookUserDoNotExist = 50007
  case accessTokenInvalid = 50008
  case refreshTokenInvalid = 50009
  case resetPasswordTokenInvalid = 50010
  case googleTokenInvalid = 50011
  case facebookTokenInvalid = 50012
  case resetPasswordUserNotFound = 50013
  case facebookUserDoNotExistAndEmailUnavailable = 50014
  case needResetPassword = 50015
  case authProviderInvalid = 50016
}
