//
//  POAuth.swift
//  POAuth
//
//  Created by Kefei Qian on 2/19/17.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import CocoaLumberjack
import CryptoSwift
import Foundation
import KeychainSwift
import POCore
// import POHelper
import RxSwift
import SwiftyJSON

public typealias UserId = String
public typealias Email = String
public typealias Password = String
public typealias Username = String
public typealias AvatarUrl = String
public typealias Bio = String
public typealias AccessToken = String
public typealias RefreshToken = String

public let poauthPasswordMaximumCharacters = 32
public let poauthUsernameMinimumCharacters = 5
public let poauthPasswordMinimumCharacters = 6
public let poauthUsernameMaximumCharacters = 32

private let POAuthStateDidChangeNotificationName = "POAuthStateDidChangeNotification"
internal let POCurrentUserAccessTokenDidChangeNotificationName = Notification.Name("POCurrentUserAccessTokenDidChangeNotification")
internal let POCurrentUserInfoDidChangeNotificationName = Notification.Name("POCurrentUserInfoDidChangeNotification")

private let poauthTokenRefreshHeadStart: Double = 5 * 60

public typealias POAuthStateDidChangeListenerBlock = (POAuth, POAuthUser?) -> Void
public typealias POAuthStateDidChangeListenerHandle = NSObjectProtocol

enum AuthKeyName: String {
  case currentUser
  case userId
  case token
  case refreshToken
  case accessToken
  case username
  case avatarUrl
  case relations
  case relationType = "type"
  case systemInfo = "system_info"
  case userInput
  case password
  case email
  case errorCode = "code"
}

public final class POAuth: NSObject {
  public static let auth = POAuth()

  public private(set) var currentUser: POAuthUser?

  private var keychain: KeychainSwift!

  private let dispatchSemaphore = DispatchSemaphore(value: 1)

  private var authStateDidChangeListenerHandles = NSMutableArray(array: [POAuthStateDidChangeListenerHandle]())

  private let disposeBag = DisposeBag()

  private var autoRefreshTokenScheduled: Bool = false

  private override init() {
    keychain = KeychainSwift(keyPrefix: POApp.app.appName)
    if let currentUserEncodedData = keychain.getData("\(AuthKeyName.currentUser.rawValue)") {
      do {
        let user = try JSONDecoder().decode(POAuthUser.self, from: currentUserEncodedData)
        currentUser = user
        super.init()
        if !autoRefreshTokenScheduled {
          scheduleAutoTokenRefresh()
        }

        DDLogDebug("User retrieved from Keychiain: \(user)")
      } catch let error {
        DDLogError(error.localizedDescription)
        currentUser = nil
        super.init()
      }
    } else {
      currentUser = nil
      super.init()
    }

    NotificationCenter.default.addObserver(self, selector: #selector(updateCurrentUserData), name: POCurrentUserAccessTokenDidChangeNotificationName, object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(updateCurrentUserData), name: POCurrentUserInfoDidChangeNotificationName, object: nil)
  }

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  private func updateCurrentUser(_ user: POAuthUser?) throws -> Bool {
    if user == currentUser {
      return true
    }

    currentUser = user
    let updateSuccess = try saveUserToDisk(user)

    if updateSuccess {
      postAuthStateChangeNotification()
    }

    return updateSuccess
  }

  private func saveUserToDisk(_ user: POAuthUser?) throws -> Bool {
    dispatchSemaphore.wait(); defer { dispatchSemaphore.signal() }
    if let user = user {
      let userEncoded = try JSONEncoder().encode(user)
      return keychain.set(userEncoded, forKey: "\(AuthKeyName.currentUser.rawValue)")
    } else {
      return keychain.delete("\(AuthKeyName.currentUser.rawValue)")
    }
  }

  @objc private func updateCurrentUserData() {
    do {
      _ = try saveUserToDisk(currentUser)
//      DDLogInfo("New user accessToken saved to keychain")
    } catch let error {
      DDLogError(error.localizedDescription)
    }
  }

  public func login(with username: Username, password: Password) -> Observable<Bool> {
    let dataDic = [
      AuthKeyName.userInput.rawValue: username,
      AuthKeyName.password.rawValue: password.sha3(.sha256),
      AuthKeyName.systemInfo.rawValue: POApp.app.systemInfo,
    ] as [String: Any]

    guard let secretData = try? JSON(dataDic).rawData(),
      let secret = POAuthCryptoManager.encryptRSA(secretData) else {
      DDLogError("Cannot encrypt rsa data:\(dataDic)")
      return Observable.error(POAuthError.rsaEncryptedError)
    }

    return POAuthLoginAPIManager(secret: secret)
      .exec()
      .map(updateCurrentUser)
  }

  public func signUp(with email: Email, username: Username, password: Password) -> Observable<Bool> {
    let dataDic = [
      AuthKeyName.email.rawValue: email,
      AuthKeyName.username.rawValue: username,
      AuthKeyName.password.rawValue: password.sha3(.sha256),
      AuthKeyName.systemInfo.rawValue: POApp.app.systemInfo,
    ] as [String: Any]

    guard let secretData = try? JSON(dataDic).rawData(),
      let secret = POAuthCryptoManager.encryptRSA(secretData) else {
      DDLogError("Cannot encrypt rsa data:\(dataDic)")
      return Observable.error(POAuthError.rsaEncryptedError)
    }

    return POAuthSignUpAPIManager(secret: secret)
      .exec()
      .map(updateCurrentUser)
  }

  public func resetPassword(of username: Username) -> Observable<Bool> {
    let dataDic = [
      AuthKeyName.userInput.rawValue: username,
      AuthKeyName.systemInfo.rawValue: POApp.app.systemInfo,
    ] as [String: Any]

    guard let secretData = try? JSON(dataDic).rawData(),
      let secret = POAuthCryptoManager.encryptRSA(secretData) else {
      DDLogError("Cannot encrypt rsa data:\(dataDic)")
      return Observable.error(POAuthError.rsaEncryptedError)
    }

    return POAuthResetPasswordAPIManager(secret: secret).exec()
  }

  public func signOut() throws -> Bool {
    if currentUser == nil {
      return true
    }

    let isSignOutSuccess = try updateCurrentUser(nil)
    return isSignOutSuccess
  }

  private func postAuthStateChangeNotification() {
    if !autoRefreshTokenScheduled {
      scheduleAutoTokenRefresh()
    }

    let notificationName = Notification.Name(POAuthStateDidChangeNotificationName)
    DispatchQueue.main.async {
      NotificationCenter.default.post(name: notificationName, object: self)
    }
  }

  public func addAuthStateDidChangeListener(_ listener: @escaping POAuthStateDidChangeListenerBlock) {
    let notificationName = Notification.Name(POAuthStateDidChangeNotificationName)
    let handle = NotificationCenter.default.addObserver(forName: notificationName,
                                                        object: self,
                                                        queue: OperationQueue.main) { notification in
      if let auth = notification.object as? POAuth {
        listener(auth, auth.currentUser)
      } else {
        DDLogError("Cannot parse AuthStateDidChange Notification Object")
      }
    }

    objc_sync_enter(self)
    authStateDidChangeListenerHandles.add(handle)
    objc_sync_exit(self)
  }

  public func removeAuthStateDidChangeListener(_ listenerHandle: POAuthStateDidChangeListenerHandle) {
    NotificationCenter.default.removeObserver(listenerHandle)
    objc_sync_enter(self)
    authStateDidChangeListenerHandles.remove(listenerHandle)
    objc_sync_exit(self)
  }

  private func scheduleAutoTokenRefresh(retryInterval: TimeInterval = 0) {
    guard let currentUser = currentUser else {
      return
    }
    let timeInterval = max(currentUser.token.accessTokenExpirationDate.timeIntervalSinceNow - poauthTokenRefreshHeadStart, 0)
    autoRefreshTokenScheduled = true

//    DDLogInfo("Auto Refresh Token Scheduled - \((DispatchTime.now() + retryInterval + timeInterval).rawValue)")
    DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + retryInterval + timeInterval) {
      self.autoRefreshTokenScheduled = false
      self.currentUser?.refreshAccessToken()
        .asObservable()
        .subscribe { event in
          switch event {
          case .completed:
            break
          case .next:
//            DDLogInfo("Token Refreshed:\(currentUser.accessToken) - \(currentUser.token.accessTokenExpirationDate.timeIntervalSinceNow)")
            self.scheduleAutoTokenRefresh()
          case let .error(error):
            DDLogError(error.localizedDescription)
            self.scheduleAutoTokenRefresh(retryInterval: 60)
          }
        }.disposed(by: self.disposeBag)
    }
  }
}
