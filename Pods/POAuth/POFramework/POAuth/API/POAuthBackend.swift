
import Apollo
import POCore

final class POAuthBackend {
  public static let sharedInstance = POAuthBackend()

  private let apollo: ApolloClient

  // MARK: - Lifecycle

  private init() {
    apollo = ApolloClient(url: POApp.app.graphQLEndpoint)
  }

  public func fetch<Query: GraphQLQuery>(query: Query, cachePolicy: CachePolicy = .fetchIgnoringCacheData, resultHandler: OperationResultHandler<Query>? = nil) -> Cancellable {
    return apollo.fetch(query: query, cachePolicy: cachePolicy, queue: DispatchQueue.global(qos: .background), resultHandler: resultHandler)
  }

  public func perform<Mutation: GraphQLMutation>(mutation: Mutation, resultHandler: OperationResultHandler<Mutation>? = nil) -> Cancellable {
    return apollo.perform(mutation: mutation, queue: DispatchQueue.global(qos: .background), resultHandler: resultHandler)
  }
}
