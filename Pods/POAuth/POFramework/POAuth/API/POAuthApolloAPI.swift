//  This file was automatically generated and should not be edited.

import Apollo

public enum RelationType: RawRepresentable, Equatable, Apollo.JSONDecodable, Apollo.JSONEncodable {
  public typealias RawValue = String
  case myself
  case block
  case stranger
  case following
  case friend
  /// Auto generated constant for unknown enum values
  case __unknown(RawValue)

  public init?(rawValue: RawValue) {
    switch rawValue {
    case "myself": self = .myself
    case "block": self = .block
    case "stranger": self = .stranger
    case "following": self = .following
    case "friend": self = .friend
    default: self = .__unknown(rawValue)
    }
  }

  public var rawValue: RawValue {
    switch self {
    case .myself: return "myself"
    case .block: return "block"
    case .stranger: return "stranger"
    case .following: return "following"
    case .friend: return "friend"
    case let .__unknown(value): return value
    }
  }

  public static func == (lhs: RelationType, rhs: RelationType) -> Bool {
    switch (lhs, rhs) {
    case (.myself, .myself): return true
    case (.block, .block): return true
    case (.stranger, .stranger): return true
    case (.following, .following): return true
    case (.friend, .friend): return true
    case let (.__unknown(lhsValue), .__unknown(rhsValue)): return lhsValue == rhsValue
    default: return false
    }
  }
}

public final class PoAuthLoginQuery: GraphQLQuery {
  public static let operationString =
    "query POAuthLogin($secret: String!) {\n  login(secret: $secret) {\n    __typename\n    ...userData\n  }\n}"

  public static var requestString: String { return operationString.appending(UserData.fragmentString) }

  public var secret: String

  public init(secret: String) {
    self.secret = secret
  }

  public var variables: GraphQLMap? {
    return ["secret": secret]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("login", arguments: ["secret": GraphQLVariable("secret")], type: .nonNull(.object(Login.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(login: Login) {
      self.init(snapshot: ["__typename": "Query", "login": login.snapshot])
    }

    public var login: Login {
      get {
        return Login(snapshot: snapshot["login"]! as! Snapshot)
      }
      set {
        snapshot.updateValue(newValue.snapshot, forKey: "login")
      }
    }

    public struct Login: GraphQLSelectionSet {
      public static let possibleTypes = ["AuthInfo"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user", type: .nonNull(.object(User.selections))),
        GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(user: User, refreshToken: String, accessToken: String) {
        self.init(snapshot: ["__typename": "AuthInfo", "user": user.snapshot, "refreshToken": refreshToken, "accessToken": accessToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var user: User {
        get {
          return User(snapshot: snapshot["user"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "user")
        }
      }

      public var refreshToken: String {
        get {
          return snapshot["refreshToken"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public var accessToken: String {
        get {
          return snapshot["accessToken"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(snapshot: snapshot)
        }
        set {
          snapshot += newValue.snapshot
        }
      }

      public struct Fragments {
        public var snapshot: Snapshot

        public var userData: UserData {
          get {
            return UserData(snapshot: snapshot)
          }
          set {
            snapshot += newValue.snapshot
          }
        }
      }

      public struct User: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("username", type: .nonNull(.scalar(String.self))),
          GraphQLField("avatarUrl", type: .nonNull(.scalar(String.self))),
          GraphQLField("relations", type: .nonNull(.object(Relation.selections))),
          GraphQLField("bio", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(userId: String, username: String, avatarUrl: String, relations: Relation, bio: String? = nil) {
          self.init(snapshot: ["__typename": "User", "userId": userId, "username": username, "avatarUrl": avatarUrl, "relations": relations.snapshot, "bio": bio])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return snapshot["userId"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "userId")
          }
        }

        public var username: String {
          get {
            return snapshot["username"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "username")
          }
        }

        public var avatarUrl: String {
          get {
            return snapshot["avatarUrl"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "avatarUrl")
          }
        }

        public var relations: Relation {
          get {
            return Relation(snapshot: snapshot["relations"]! as! Snapshot)
          }
          set {
            snapshot.updateValue(newValue.snapshot, forKey: "relations")
          }
        }

        public var bio: String? {
          get {
            return snapshot["bio"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "bio")
          }
        }

        public struct Relation: GraphQLSelectionSet {
          public static let possibleTypes = ["UserRelations"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("followingAmount", type: .nonNull(.scalar(Int.self))),
            GraphQLField("followerAmount", type: .nonNull(.scalar(Int.self))),
            GraphQLField("type", type: .nonNull(.scalar(RelationType.self))),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(followingAmount: Int, followerAmount: Int, type: RelationType) {
            self.init(snapshot: ["__typename": "UserRelations", "followingAmount": followingAmount, "followerAmount": followerAmount, "type": type])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var followingAmount: Int {
            get {
              return snapshot["followingAmount"]! as! Int
            }
            set {
              snapshot.updateValue(newValue, forKey: "followingAmount")
            }
          }

          public var followerAmount: Int {
            get {
              return snapshot["followerAmount"]! as! Int
            }
            set {
              snapshot.updateValue(newValue, forKey: "followerAmount")
            }
          }

          public var type: RelationType {
            get {
              return snapshot["type"]! as! RelationType
            }
            set {
              snapshot.updateValue(newValue, forKey: "type")
            }
          }
        }
      }
    }
  }
}

public final class PoAuthSignUpMutation: GraphQLMutation {
  public static let operationString =
    "mutation POAuthSignUp($secret: String!) {\n  register(secret: $secret) {\n    __typename\n    ...userData\n  }\n}"

  public static var requestString: String { return operationString.appending(UserData.fragmentString) }

  public var secret: String

  public init(secret: String) {
    self.secret = secret
  }

  public var variables: GraphQLMap? {
    return ["secret": secret]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("register", arguments: ["secret": GraphQLVariable("secret")], type: .nonNull(.object(Register.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(register: Register) {
      self.init(snapshot: ["__typename": "Mutation", "register": register.snapshot])
    }

    public var register: Register {
      get {
        return Register(snapshot: snapshot["register"]! as! Snapshot)
      }
      set {
        snapshot.updateValue(newValue.snapshot, forKey: "register")
      }
    }

    public struct Register: GraphQLSelectionSet {
      public static let possibleTypes = ["AuthInfo"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user", type: .nonNull(.object(User.selections))),
        GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(user: User, refreshToken: String, accessToken: String) {
        self.init(snapshot: ["__typename": "AuthInfo", "user": user.snapshot, "refreshToken": refreshToken, "accessToken": accessToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var user: User {
        get {
          return User(snapshot: snapshot["user"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "user")
        }
      }

      public var refreshToken: String {
        get {
          return snapshot["refreshToken"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public var accessToken: String {
        get {
          return snapshot["accessToken"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(snapshot: snapshot)
        }
        set {
          snapshot += newValue.snapshot
        }
      }

      public struct Fragments {
        public var snapshot: Snapshot

        public var userData: UserData {
          get {
            return UserData(snapshot: snapshot)
          }
          set {
            snapshot += newValue.snapshot
          }
        }
      }

      public struct User: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("username", type: .nonNull(.scalar(String.self))),
          GraphQLField("avatarUrl", type: .nonNull(.scalar(String.self))),
          GraphQLField("relations", type: .nonNull(.object(Relation.selections))),
          GraphQLField("bio", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(userId: String, username: String, avatarUrl: String, relations: Relation, bio: String? = nil) {
          self.init(snapshot: ["__typename": "User", "userId": userId, "username": username, "avatarUrl": avatarUrl, "relations": relations.snapshot, "bio": bio])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return snapshot["userId"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "userId")
          }
        }

        public var username: String {
          get {
            return snapshot["username"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "username")
          }
        }

        public var avatarUrl: String {
          get {
            return snapshot["avatarUrl"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "avatarUrl")
          }
        }

        public var relations: Relation {
          get {
            return Relation(snapshot: snapshot["relations"]! as! Snapshot)
          }
          set {
            snapshot.updateValue(newValue.snapshot, forKey: "relations")
          }
        }

        public var bio: String? {
          get {
            return snapshot["bio"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "bio")
          }
        }

        public struct Relation: GraphQLSelectionSet {
          public static let possibleTypes = ["UserRelations"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("followingAmount", type: .nonNull(.scalar(Int.self))),
            GraphQLField("followerAmount", type: .nonNull(.scalar(Int.self))),
            GraphQLField("type", type: .nonNull(.scalar(RelationType.self))),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(followingAmount: Int, followerAmount: Int, type: RelationType) {
            self.init(snapshot: ["__typename": "UserRelations", "followingAmount": followingAmount, "followerAmount": followerAmount, "type": type])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var followingAmount: Int {
            get {
              return snapshot["followingAmount"]! as! Int
            }
            set {
              snapshot.updateValue(newValue, forKey: "followingAmount")
            }
          }

          public var followerAmount: Int {
            get {
              return snapshot["followerAmount"]! as! Int
            }
            set {
              snapshot.updateValue(newValue, forKey: "followerAmount")
            }
          }

          public var type: RelationType {
            get {
              return snapshot["type"]! as! RelationType
            }
            set {
              snapshot.updateValue(newValue, forKey: "type")
            }
          }
        }
      }
    }
  }
}

public final class PoAuthRenewAccessTokenQuery: GraphQLQuery {
  public static let operationString =
    "query POAuthRenewAccessToken($secret: String!) {\n  renewAccessToken(secret: $secret) {\n    __typename\n    ...userData\n  }\n}"

  public static var requestString: String { return operationString.appending(UserData.fragmentString) }

  public var secret: String

  public init(secret: String) {
    self.secret = secret
  }

  public var variables: GraphQLMap? {
    return ["secret": secret]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("renewAccessToken", arguments: ["secret": GraphQLVariable("secret")], type: .nonNull(.object(RenewAccessToken.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(renewAccessToken: RenewAccessToken) {
      self.init(snapshot: ["__typename": "Query", "renewAccessToken": renewAccessToken.snapshot])
    }

    public var renewAccessToken: RenewAccessToken {
      get {
        return RenewAccessToken(snapshot: snapshot["renewAccessToken"]! as! Snapshot)
      }
      set {
        snapshot.updateValue(newValue.snapshot, forKey: "renewAccessToken")
      }
    }

    public struct RenewAccessToken: GraphQLSelectionSet {
      public static let possibleTypes = ["AuthInfo"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("user", type: .nonNull(.object(User.selections))),
        GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
        GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(user: User, refreshToken: String, accessToken: String) {
        self.init(snapshot: ["__typename": "AuthInfo", "user": user.snapshot, "refreshToken": refreshToken, "accessToken": accessToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var user: User {
        get {
          return User(snapshot: snapshot["user"]! as! Snapshot)
        }
        set {
          snapshot.updateValue(newValue.snapshot, forKey: "user")
        }
      }

      public var refreshToken: String {
        get {
          return snapshot["refreshToken"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "refreshToken")
        }
      }

      public var accessToken: String {
        get {
          return snapshot["accessToken"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "accessToken")
        }
      }

      public var fragments: Fragments {
        get {
          return Fragments(snapshot: snapshot)
        }
        set {
          snapshot += newValue.snapshot
        }
      }

      public struct Fragments {
        public var snapshot: Snapshot

        public var userData: UserData {
          get {
            return UserData(snapshot: snapshot)
          }
          set {
            snapshot += newValue.snapshot
          }
        }
      }

      public struct User: GraphQLSelectionSet {
        public static let possibleTypes = ["User"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("userId", type: .nonNull(.scalar(String.self))),
          GraphQLField("username", type: .nonNull(.scalar(String.self))),
          GraphQLField("avatarUrl", type: .nonNull(.scalar(String.self))),
          GraphQLField("relations", type: .nonNull(.object(Relation.selections))),
          GraphQLField("bio", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(userId: String, username: String, avatarUrl: String, relations: Relation, bio: String? = nil) {
          self.init(snapshot: ["__typename": "User", "userId": userId, "username": username, "avatarUrl": avatarUrl, "relations": relations.snapshot, "bio": bio])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var userId: String {
          get {
            return snapshot["userId"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "userId")
          }
        }

        public var username: String {
          get {
            return snapshot["username"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "username")
          }
        }

        public var avatarUrl: String {
          get {
            return snapshot["avatarUrl"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "avatarUrl")
          }
        }

        public var relations: Relation {
          get {
            return Relation(snapshot: snapshot["relations"]! as! Snapshot)
          }
          set {
            snapshot.updateValue(newValue.snapshot, forKey: "relations")
          }
        }

        public var bio: String? {
          get {
            return snapshot["bio"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "bio")
          }
        }

        public struct Relation: GraphQLSelectionSet {
          public static let possibleTypes = ["UserRelations"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("followingAmount", type: .nonNull(.scalar(Int.self))),
            GraphQLField("followerAmount", type: .nonNull(.scalar(Int.self))),
            GraphQLField("type", type: .nonNull(.scalar(RelationType.self))),
          ]

          public var snapshot: Snapshot

          public init(snapshot: Snapshot) {
            self.snapshot = snapshot
          }

          public init(followingAmount: Int, followerAmount: Int, type: RelationType) {
            self.init(snapshot: ["__typename": "UserRelations", "followingAmount": followingAmount, "followerAmount": followerAmount, "type": type])
          }

          public var __typename: String {
            get {
              return snapshot["__typename"]! as! String
            }
            set {
              snapshot.updateValue(newValue, forKey: "__typename")
            }
          }

          public var followingAmount: Int {
            get {
              return snapshot["followingAmount"]! as! Int
            }
            set {
              snapshot.updateValue(newValue, forKey: "followingAmount")
            }
          }

          public var followerAmount: Int {
            get {
              return snapshot["followerAmount"]! as! Int
            }
            set {
              snapshot.updateValue(newValue, forKey: "followerAmount")
            }
          }

          public var type: RelationType {
            get {
              return snapshot["type"]! as! RelationType
            }
            set {
              snapshot.updateValue(newValue, forKey: "type")
            }
          }
        }
      }
    }
  }
}

public final class PoAuthRequestResetPasswordQuery: GraphQLQuery {
  public static let operationString =
    "query POAuthRequestResetPassword($secret: String!) {\n  requestResetPassword(secret: $secret) {\n    __typename\n    isSucceed\n  }\n}"

  public var secret: String

  public init(secret: String) {
    self.secret = secret
  }

  public var variables: GraphQLMap? {
    return ["secret": secret]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("requestResetPassword", arguments: ["secret": GraphQLVariable("secret")], type: .nonNull(.object(RequestResetPassword.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(requestResetPassword: RequestResetPassword) {
      self.init(snapshot: ["__typename": "Query", "requestResetPassword": requestResetPassword.snapshot])
    }

    public var requestResetPassword: RequestResetPassword {
      get {
        return RequestResetPassword(snapshot: snapshot["requestResetPassword"]! as! Snapshot)
      }
      set {
        snapshot.updateValue(newValue.snapshot, forKey: "requestResetPassword")
      }
    }

    public struct RequestResetPassword: GraphQLSelectionSet {
      public static let possibleTypes = ["StatusResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("isSucceed", type: .nonNull(.scalar(Bool.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(isSucceed: Bool) {
        self.init(snapshot: ["__typename": "StatusResponse", "isSucceed": isSucceed])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var isSucceed: Bool {
        get {
          return snapshot["isSucceed"]! as! Bool
        }
        set {
          snapshot.updateValue(newValue, forKey: "isSucceed")
        }
      }
    }
  }
}

public struct UserData: GraphQLFragment {
  public static let fragmentString =
    "fragment userData on AuthInfo {\n  __typename\n  user {\n    __typename\n    userId\n    username\n    avatarUrl\n    relations {\n      __typename\n      followingAmount\n      followerAmount\n      type\n    }\n    bio\n  }\n  refreshToken\n  accessToken\n}"

  public static let possibleTypes = ["AuthInfo"]

  public static let selections: [GraphQLSelection] = [
    GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
    GraphQLField("user", type: .nonNull(.object(User.selections))),
    GraphQLField("refreshToken", type: .nonNull(.scalar(String.self))),
    GraphQLField("accessToken", type: .nonNull(.scalar(String.self))),
  ]

  public var snapshot: Snapshot

  public init(snapshot: Snapshot) {
    self.snapshot = snapshot
  }

  public init(user: User, refreshToken: String, accessToken: String) {
    self.init(snapshot: ["__typename": "AuthInfo", "user": user.snapshot, "refreshToken": refreshToken, "accessToken": accessToken])
  }

  public var __typename: String {
    get {
      return snapshot["__typename"]! as! String
    }
    set {
      snapshot.updateValue(newValue, forKey: "__typename")
    }
  }

  public var user: User {
    get {
      return User(snapshot: snapshot["user"]! as! Snapshot)
    }
    set {
      snapshot.updateValue(newValue.snapshot, forKey: "user")
    }
  }

  public var refreshToken: String {
    get {
      return snapshot["refreshToken"]! as! String
    }
    set {
      snapshot.updateValue(newValue, forKey: "refreshToken")
    }
  }

  public var accessToken: String {
    get {
      return snapshot["accessToken"]! as! String
    }
    set {
      snapshot.updateValue(newValue, forKey: "accessToken")
    }
  }

  public struct User: GraphQLSelectionSet {
    public static let possibleTypes = ["User"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
      GraphQLField("userId", type: .nonNull(.scalar(String.self))),
      GraphQLField("username", type: .nonNull(.scalar(String.self))),
      GraphQLField("avatarUrl", type: .nonNull(.scalar(String.self))),
      GraphQLField("relations", type: .nonNull(.object(Relation.selections))),
      GraphQLField("bio", type: .scalar(String.self)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(userId: String, username: String, avatarUrl: String, relations: Relation, bio: String? = nil) {
      self.init(snapshot: ["__typename": "User", "userId": userId, "username": username, "avatarUrl": avatarUrl, "relations": relations.snapshot, "bio": bio])
    }

    public var __typename: String {
      get {
        return snapshot["__typename"]! as! String
      }
      set {
        snapshot.updateValue(newValue, forKey: "__typename")
      }
    }

    public var userId: String {
      get {
        return snapshot["userId"]! as! String
      }
      set {
        snapshot.updateValue(newValue, forKey: "userId")
      }
    }

    public var username: String {
      get {
        return snapshot["username"]! as! String
      }
      set {
        snapshot.updateValue(newValue, forKey: "username")
      }
    }

    public var avatarUrl: String {
      get {
        return snapshot["avatarUrl"]! as! String
      }
      set {
        snapshot.updateValue(newValue, forKey: "avatarUrl")
      }
    }

    public var relations: Relation {
      get {
        return Relation(snapshot: snapshot["relations"]! as! Snapshot)
      }
      set {
        snapshot.updateValue(newValue.snapshot, forKey: "relations")
      }
    }

    public var bio: String? {
      get {
        return snapshot["bio"] as? String
      }
      set {
        snapshot.updateValue(newValue, forKey: "bio")
      }
    }

    public struct Relation: GraphQLSelectionSet {
      public static let possibleTypes = ["UserRelations"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("followingAmount", type: .nonNull(.scalar(Int.self))),
        GraphQLField("followerAmount", type: .nonNull(.scalar(Int.self))),
        GraphQLField("type", type: .nonNull(.scalar(RelationType.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(followingAmount: Int, followerAmount: Int, type: RelationType) {
        self.init(snapshot: ["__typename": "UserRelations", "followingAmount": followingAmount, "followerAmount": followerAmount, "type": type])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var followingAmount: Int {
        get {
          return snapshot["followingAmount"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "followingAmount")
        }
      }

      public var followerAmount: Int {
        get {
          return snapshot["followerAmount"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "followerAmount")
        }
      }

      public var type: RelationType {
        get {
          return snapshot["type"]! as! RelationType
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }
    }
  }
}
