import RxSwift

protocol POAuthAPIProtocol {
  var secret: String { get }
  associatedtype T
  func exec() -> Observable<T>
}
