//
//  POAuthSingUpAPIManager.swift
//  POAuth
//
//  Created by Kefei Qian on 19/2/18.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import Apollo
import RxSwift
import SwiftyJSON

final class POAuthSignUpAPIManager: POAuthAPIProtocol {
  let secret: String

  init(secret: String) {
    self.secret = secret
  }

  func exec() -> Observable<POAuthUser> {
    let mutation = PoAuthSignUpMutation(secret: secret)
    return Observable.create { (observer) -> Disposable in
      let request = POAuthBackend.sharedInstance.perform(mutation: mutation) { result, error in

        if let error = error {
          observer.onError(error)
        } else if let errors = result?.errors {
          errors.forEach { error in
            let authError = POAuthHelper.parsePOAuthErrorFromGraphQLResponse(error)
            observer.onError(authError)
          }
        } else if let result = result,
          let userData = result.data?.register.user.jsonObject,
          let refreshToken = result.data?.register.refreshToken,
          let accessToken = result.data?.register.accessToken,
          let user = POAuthUser(userInfo: JSON(userData), refreshToken: refreshToken, accessToken: accessToken) {
          observer.onNext(user)
          observer.onCompleted()
        } else {
          observer.onError(POAuthError.invalidServerResponse)
        }
      }

      return Disposables.create {
        request.cancel()
      }
    }
  }
}
