//
//  POAuthLoginAPIManager.swift
//  POAuth
//
//  Created by Kefei Qian on 19/2/18.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import Apollo
import RxSwift
import SwiftyJSON

final class POAuthResetPasswordAPIManager: POAuthAPIProtocol {
  let secret: String

  init(secret: String) {
    self.secret = secret
  }

  func exec() -> Observable<Bool> {
    let query = PoAuthRequestResetPasswordQuery(secret: secret)
    return Observable.create { (observer) -> Disposable in
      let request = POAuthBackend.sharedInstance.fetch(query: query) { result, error in
        if let error = error {
          observer.onError(error)
        } else if let errors = result?.errors {
          errors.forEach { error in
            let authError = POAuthHelper.parsePOAuthErrorFromGraphQLResponse(error)
            observer.onError(authError)
          }
        } else if let result = result,
          let isSucceed = result.data?.requestResetPassword.isSucceed {
          observer.onNext(isSucceed)
          observer.onCompleted()
        } else {
          observer.onError(POAuthError.invalidServerResponse)
        }
      }

      return Disposables.create {
        request.cancel()
      }
    }
  }
}
