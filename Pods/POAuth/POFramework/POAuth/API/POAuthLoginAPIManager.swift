//
//  POAuthLoginAPIManager.swift
//  POAuth
//
//  Created by Kefei Qian on 19/2/18.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import Apollo
import POCore
import RxSwift
import SwiftyJSON

final class POAuthLoginAPIManager: POAuthAPIProtocol {
  let secret: String

  init(secret: String) {
    self.secret = secret
  }

  func exec() -> Observable<POAuthUser> {
    let query = PoAuthLoginQuery(secret: secret)
    return Observable.create { (observer) -> Disposable in
      let request = POAuthBackend.sharedInstance.fetch(query: query) { result, error in
        if let error = error {
          observer.onError(error)
        } else if let errors = result?.errors {
          errors.forEach { error in
            let authError = POAuthHelper.parsePOAuthErrorFromGraphQLResponse(error)
            observer.onError(authError)
          }
        } else if let result = result,
          let userData = result.data?.login.user.jsonObject,
          let refreshToken = result.data?.login.refreshToken,
          let accessToken = result.data?.login.accessToken,
          let user = POAuthUser(userInfo: JSON(userData), refreshToken: refreshToken, accessToken: accessToken) {
          observer.onNext(user)
          observer.onCompleted()
        } else {
          observer.onError(POAuthError.invalidServerResponse)
        }
      }
      return Disposables.create {
        request.cancel()
      }
    }
  }
}
