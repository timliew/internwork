
import CocoaLumberjack
import Foundation
import POCore
import RxSwift
import SwiftyJSON

final class POAuthTokenService: NSObject, Codable {
  var accessToken: AccessToken
  let refreshToken: RefreshToken
  var accessTokenExpirationDate: Date {
    return POAuthHelper.parseAccessTokenExpireDate(accessToken)
  }

  private let userId: UserId

  private let lock: NSLock = NSLock()
  private let disposeBag = DisposeBag()

  required init(refreshToken: RefreshToken, accessToken: AccessToken, userId: UserId) {
    self.accessToken = accessToken
    self.refreshToken = refreshToken
    self.userId = userId
  }

  required init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    accessToken = try values.decode(String.self, forKey: .accessToken)
    refreshToken = try values.decode(String.self, forKey: .refreshToken)
    userId = try values.decode(String.self, forKey: .userId)
  }

  func refreshAccessToken(force: Bool = false) -> Observable<Bool> {
    return Observable.create { (observer) -> Disposable in
      self.lock.lock(); defer { self.lock.unlock() }
      if self.hasValidAccessToken() && !force {
        observer.onNext(true)
        observer.onCompleted()
      } else {
        self.requestAccessToken { isSuccess, error in
          if let error = error {
            observer.onError(error)
            switch error {
            case POAuthError.refreshTokenInvalid:
              _ = try? POAuth.auth.signOut()
            default:
              break
            }
          } else {
            observer.onNext(isSuccess)
            observer.onCompleted()
          }
        }
      }
      return Disposables.create {}
    }
  }

  private func requestAccessToken(_ completion: @escaping (Bool, Error?) -> Void) {
    let dataDic = [
      AuthKeyName.userInput.rawValue: userId,
      AuthKeyName.token.rawValue: refreshToken,
      AuthKeyName.systemInfo.rawValue: POApp.app.systemInfo,
    ] as [String: Any]

    guard let secretData = try? JSON(dataDic).rawData(),
      let secret = POAuthCryptoManager.encryptRSA(secretData) else {
      DDLogError("Cannot encrypt rsa data:\(dataDic)")
      completion(false, POAuthError.rsaEncryptedError)
      return
    }

    POAuthRenewAccessTokenAPIManager(secret: secret).exec()
      .asObservable()
      .subscribe { event in
        switch event {
        case .completed:
          completion(true, nil)
        case let .next(accessToken):
          self.accessToken = accessToken
        case let .error(error):
          completion(false, error)
        }
      }.disposed(by: disposeBag)
  }

  private func hasValidAccessToken() -> Bool {
    return true
  }
}

extension POAuthTokenService {
  enum CodingKeys: String, CodingKey {
    case accessToken
    case refreshToken
    case userId
  }

  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(accessToken, forKey: .accessToken)
    try container.encode(refreshToken, forKey: .refreshToken)
    try container.encode(userId, forKey: .userId)
  }
}
