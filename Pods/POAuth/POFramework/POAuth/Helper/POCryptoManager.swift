//
//  POCryptoManger.swift
//  POAuth
//
//  Created by Kefei Qian on 2/19/18.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import CocoaLumberjack
import SwiftyJSON
import SwiftyRSA

final class POAuthCryptoManager {
  private static let aesKey = Array("ysuH8kxV7DL2nvTWaDF}iKtDHohugTFK".utf8)

  private static let publicKey = """
  MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0jTkSDfW4M5OjRaCTyT4
  adYfVAmZ1i4rkY8rSNH3kFpU2yk11wVwJQRJuFKWNt/CIaANy7y8lV6oEQkmVx7L
  sOZwAJfmi7NtuvFkWLUcMa/lpUJMXm3lSOI5FEMRMQYGuWAZJKxHNET1vwCxt8te
  FAYEZ+5Z+evcrjU0eqHRF+HjGgcK5tIaUM0kEEoga9NZNqqrwRyZ9ABKfwk2LRdY
  iOqA4f9LjRgVqUeopDcG+hPbUcZkeGhQlaZgow0xyfgb23hWkmz3mChczNaaPfbi
  1Uu02N0VGZum+Iea/afIuGFOv3hN7k8ASPUYyVFjg3jXIpJ5qSruUZqnZ9UfjMT0
  lQIDAQAB
  """

  static func encryptRSA(_ data: Data) -> String? {
    guard let publicKey = try? PublicKey(base64Encoded: publicKey)
    else {
      fatalError("Can not generate public key")
    }

    let clearMessage = ClearMessage(data: data)

    do {
      let encrypted = try clearMessage.encrypted(with: publicKey, padding: .PKCS1)
      return encrypted.base64String
    } catch {
      DDLogError("Can not encrypt rsa: \(error)")
      return nil
    }
  }
}
