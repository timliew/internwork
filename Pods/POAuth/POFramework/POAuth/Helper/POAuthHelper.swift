//
//  POAuthHelper.swift
//  POAuth
//
//  Created by Kefei Qian on 2/19/18.
//  Copyright (c) 2017-present, PinOn, Inc. All rights reserved.
//

import CocoaLumberjack
import JWTDecode

final class POAuthHelper {
  static func parsePOAuthErrorFromGraphQLResponse(_ error: Error) -> POAuthError {
    print(error)
    if let data = error.localizedDescription.data(using: .utf8),
      let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any],
      let errorCode = json[AuthKeyName.errorCode.rawValue] as? Int,
      let authError = POAuthError(rawValue: errorCode) {
      return authError
    } else {
      return .unknownError
    }
  }

  static func checkAccessTokenIsExpired(_ accessToken: AccessToken) -> Bool {
    guard let jwt = try? decode(jwt: accessToken) else {
      return true
    }

    return jwt.expired
  }

  static func parseAccessTokenExpireDate(_ accessToken: AccessToken) -> Date {
    guard let jwt = try? decode(jwt: accessToken),
      let expire = jwt.expiresAt else {
      fatalError("Cannot parse Expire Date:\(accessToken)")
    }

    return expire
  }
}
