//
//  ImageStruct.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/2/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import UIKit

struct Image {
    let imageName: String!
}
