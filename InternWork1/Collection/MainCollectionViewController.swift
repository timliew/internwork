//
//  MainCollectionViewController.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/2/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import UIKit

class MainCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let reuseIdentifier = "Cell"
    
    private let arr = [
        Image(imageName: "jeff"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "jeff"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "jeff"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "elon"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "jeff"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "elon"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "jeff"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "elon"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve"),
        Image(imageName: "jeff"),Image(imageName: "tim"),
        Image(imageName: "elon"),Image(imageName: "steve")
    ]
    
    private let asyncFetcher = AsyncFetcher()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup background
        self.collectionView?.backgroundColor = .white
        self.collectionView?.alwaysBounceVertical = true
        
        // Refresh Action
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        self.collectionView?.addSubview(refreshControl)

        // Register cell classes
        self.collectionView!.register(CustomCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.navigationItem.title = "Bookmarks"
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black, NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18)]
        
        //BarButtonItem
        let notifyButton = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(notifyButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = notifyButton
    }
    
    // MARK: Collection View Setup
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CustomCell
        let cellImage = arr[indexPath.item]
        
        if let _ = asyncFetcher.fetchedData(for: cellImage.imageName) {
            cell.arr = cellImage
        } else {
            cell.arr = Image(imageName: "user")
            asyncFetcher.fetchAsync(cellImage.imageName) { fetchedData in
                DispatchQueue.main.async {
                    guard cell.arr?.imageName == fetchedData?.image else { return }
                    cell.arr = Image(imageName: fetchedData?.image)
                    self.collectionView?.reloadData()
                }
            }
        }
        
        cell.imageV.layer.cornerRadius = 5
        cell.imageV.clipsToBounds = true
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController()
        alert.addAction(UIAlertAction(title: NSLocalizedString("Follow", comment: "Default action"), style: .default, handler: { _ in
            NSLog("\"Follow\" option was selected.")
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Unfollow", comment: "Alternate action"), style: .default, handler: { _ in
            NSLog("\"Unfollow\" option was selected.")
        }))
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            NSLog("\"Cancel\" option was selected.")
        })
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeFrame = (view.frame.width - 24)/3
        return CGSize(width: sizeFrame, height: sizeFrame)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    // MARK: Notify Button Action
    @objc func notifyButtonAction(sender: UIButton!){
        let alert = UIAlertController(title: "Use Camera?", message: nil, preferredStyle: .alert)
        let noOption = UIAlertAction(title: "No", style: .default, handler: { _ in
            NSLog("\"No\" option was selected.")
        })
        noOption.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(noOption)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            NSLog("\"Yes\" option was selected.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Refresh Action
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.collectionView?.reloadData()
        refreshControl.endRefreshing()
    }
}

extension MainCollectionViewController: UICollectionViewDataSourcePrefetching {
    // MARK: Prefetch Setup
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            let arrItem = arr[indexPath.row]
            asyncFetcher.fetchAsync(arrItem.imageName)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        // Cancel any in-flight requests for data for the specified index paths.
        for indexPath in indexPaths {
            let arrItem = arr[indexPath.row]
            asyncFetcher.cancelFetch(arrItem.imageName)
        }
    }
}

