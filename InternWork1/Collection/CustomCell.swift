//
//  CustomCell.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/2/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {
    var arr: Image? {
        didSet {
            guard let image = arr?.imageName else { return }
            imageV.image = UIImage(named: image)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let imageV: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    func setupView() {
        self.addSubview(imageV)
        imageV.anchor(top: topAnchor, left: leftAnchor, right: rightAnchor, bottom: bottomAnchor, paddingTop: 0, paddingLeft: 0, paddingRight: 0, paddingBottom: 0, width: 0, height: 0)
    }
    
}
