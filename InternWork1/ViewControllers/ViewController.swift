//
//  ViewController.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/2/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var viewHasLaidOut = true
    
    // MARK: View Images Button
    let imagesButton: UIButton = {
        let button = UIButton()
        button.setTitle("View Images", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont(name: "Helvetica", size: 15)
        button.backgroundColor = .white
        button.layer.borderWidth = 1.5
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(imagesButtonPressed(sender:)), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.view.addSubview(imagesButton)
        
        //BarButtonItem
        let settingsButton = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(settings(sender:)))
        self.navigationItem.rightBarButtonItem = settingsButton
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if viewHasLaidOut {
            imagesButton.translatesAutoresizingMaskIntoConstraints = false
            imagesButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            imagesButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            imagesButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
            imagesButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        }
    }
    
    // MARK: Image Button Action
    @objc func imagesButtonPressed(sender: UIButton!) {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCVC = MainCollectionViewController(collectionViewLayout: flowLayout)
        self.navigationController?.pushViewController(mainCVC, animated: true)
    }
    
    @objc func settings(sender: UIButton!) {
        let asyncTable = AsyncTableViewController()
        self.navigationController?.pushViewController(asyncTable, animated: true)
    }

}

