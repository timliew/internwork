//
//  LikesViewerContract.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  COPYRIGHT © 2018-PRESENT Tim Liew ALL RIGHTS RESERVED.
//

import AsyncDisplayKit

protocol LikesViewerView: class { // View
    var presenter: LikesViewerPresentation! { get set }
    var event: LikesViewerEvent { get set }
  // If the view needs change the UIState, you can uncomment the following line
  // var state: UIState {get set}

  // TODO: Declare view methods, e.g. func showData()
  
}

protocol LikesViewerUseCase: class { // Interactor
    var output: LikesViewerInteractorOutput? { get set }
  
    // TODO: Declare use case methods, e.g. func fetchDatafromServer()
    func fetchMoreLikes(like: Likes)
    func refreshLikes(like: Likes)
}

protocol LikesViewerPresentation: class { // Presenter
  var view: LikesViewerView? { get set }
  var interactor: LikesViewerUseCase! { get set }
  var router: LikesViewerWireframe! { get set }
  
  // TODO: Declare presentation methods, e.g. func viewDidLoad() / func didPressButton()
    func didPullToRefresh()
}

protocol LikesViewerInteractorOutput: class { // Presenter
  // TODO: Declare interactor output methods, e.g. func dataFetched()
    func didFetchLikes(likes: [Likes])
    func didRefreshLikes(likes: [Likes])
}

protocol LikesViewerWireframe: class { // Router
   // TODO: Declare wireframe methods, e.g. func presentViewController()
    func showLikeImg(_ like: Likes)
    func dismiss()
}

enum LikesViewerEvent {
    case initial
    case didFetchLikes(likes: [Likes])
    case didRefreshLikes(likes: [Likes])
}
