//
//  LikesViewerCoordinator.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import AsyncDisplayKit
import PONav

protocol LikesViewerCoordinatorDelegate: class {
    func likesViewerCoordinatorShouldDismiss(_ coordinator: LikesViewerCoordinator)
    func likesViewerCoordinatorShouldShowLikesViewer(_ coordinator: LikesViewerCoordinator, like: Likes)
}

class LikesViewerCoordinator: POCoordinator {
    
    public weak var delegate: LikesViewerCoordinatorDelegate!

    private let like: Likes
    private let router: PORouter

    public required init(like: Likes, router: PORouter) {
        self.like = like
        self.router = router
        
        super.init(name: String(describing: LikesViewerCoordinator.self))
    }
    
    public override func start() {
        let view = LikesViewerViewController()
        let interactor = LikesViewerInteractor()
        let presenter = LikesViewerPresenter()

        view.presenter = presenter
        interactor.output = presenter

        presenter.interactor = interactor
        presenter.router = self
        presenter.view = view
        
        router.push(view)
    }

}

extension LikesViewerCoordinator: LikesViewerWireframe {
    func showLikeImg(_ like: Likes) {
        delegate.likesViewerCoordinatorShouldShowLikesViewer(self, like: like)
    }
    
    func dismiss() {
        delegate.likesViewerCoordinatorShouldDismiss(self)
    }
}
