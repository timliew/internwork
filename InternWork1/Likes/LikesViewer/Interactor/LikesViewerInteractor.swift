//
//  LikesViewerInteractor.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  COPYRIGHT © 2018-PRESENT Tim Liew ALL RIGHTS RESERVED.
//

import CocoaLumberjack
import AsyncDisplayKit

final class LikesViewerInteractor {

    // MARK: - Properties

    weak var output: LikesViewerInteractorOutput?

}

extension LikesViewerInteractor: LikesViewerUseCase {
    // TODO: Implement use case methods
    func fetchMoreLikes(like: Likes) {
    }
    
    func refreshLikes(like: Likes) {
    }
}
