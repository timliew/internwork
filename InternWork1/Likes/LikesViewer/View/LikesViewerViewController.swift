//
//  LikesViewerViewController.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  COPYRIGHT © 2018-PRESENT Tim Liew ALL RIGHTS RESERVED.
//

import AsyncDisplayKit
import CocoaLumberjack

final class LikesViewerViewController: ASViewController<ASDisplayNode>, ASCollectionDelegate, LikesViewerView {

    // MARK: - Properties
    
    // If the view needs change the UIState, you can uncomment the following line
    // var state: UIState {get set}
  
    // MARK: - Variables
    var presenter: LikesViewerPresentation!

    var event: LikesViewerEvent = .initial {
        didSet {
            switch event {
            case let .didFetchLikes(likes):
                collectionNode.updateLikes(likes: likes)
                break
            case let .didRefreshLikes(likes):
                collectionNode.refreshLikes(likes: likes)
                break
            default:
                break
            }
        }
    }
    
    // MARK: View Components
    private let collectionNode: ASCollectionNode & LikesViewerCollectionNodeView
    private let displayNode: ASDisplayNode & LikesViewerDisplayNodeView

    // MARK: - Lifecycle
  
    init() {
        collectionNode = LikesViewerCollectionNode(collectionViewLayout: UICollectionViewFlowLayout())
        displayNode = LikesViewerDisplayNode(collectionNode: collectionNode)
        super.init(node: displayNode)
        collectionNode.eventDelegate = self
    }
  
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  
    deinit {
        DDLogVerbose("deinit")
    }
  
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Likes"
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black, NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18)]
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
    }
}

// MARK: Delegate
extension LikesViewerViewController: LikesViewerCollectionNodeDelegate {
    func didPullToRefresh() {
//        presenter.didPullToRefresh()
        self.collectionNode.reloadData()
    }
}
