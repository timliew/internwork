//
//  LikesViewerDisplayNode.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import AsyncDisplayKit

protocol LikesViewerDisplayNodeView: class {
}

class LikesViewerDisplayNode: ASDisplayNode, LikesViewerDisplayNodeView {
    
    // MARK: View components
    private let collectionNode: ASCollectionNode
    
    // MARK: Lifestyle
    required init(collectionNode: ASCollectionNode) {
        self.collectionNode = collectionNode
        self.collectionNode.backgroundColor = .white
        super.init()
        automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        collectionNode.style.preferredSize = CGSize(width: self.view.frame.height, height: self.view.frame.height)
        
        return ASStackLayoutSpec(direction: .vertical,
                                 spacing: 0,
                                 justifyContent: .start,
                                 alignItems: .center,
                                 children: [collectionNode])
    }
}
