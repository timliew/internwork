//
//  LikesViewerCollectionNode.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import AsyncDisplayKit

protocol LikesViewerCollectionNodeDelegate: class {
    func didPullToRefresh()
}

protocol LikesViewerCollectionNodeView: class {
    var eventDelegate: LikesViewerCollectionNodeDelegate? { get set}
    
    func updateLikes(likes: [Likes])
    func refreshLikes(likes: [Likes])
}

class LikesViewerCollectionNode: ASCollectionNode, LikesViewerCollectionNodeView, ASCollectionDelegate {
    var eventDelegate: LikesViewerCollectionNodeDelegate?
    private let refreshControl = UIRefreshControl()
    private var likes = [Likes(likesUserImg: "elon"),Likes(likesUserImg: "tim"),
                         Likes(likesUserImg: "jeff"),Likes(likesUserImg: "steve"),
                         Likes(likesUserImg: "elon"),Likes(likesUserImg: "tim"),
                         Likes(likesUserImg: "jeff"),Likes(likesUserImg: "steve"),
                         Likes(likesUserImg: "elon"),Likes(likesUserImg: "tim"),
                         Likes(likesUserImg: "jeff"),Likes(likesUserImg: "steve"),
                         Likes(likesUserImg: "elon"),Likes(likesUserImg: "tim"),
                         Likes(likesUserImg: "jeff"),Likes(likesUserImg: "steve"),
                         Likes(likesUserImg: "elon"),Likes(likesUserImg: "tim"),
                         Likes(likesUserImg: "jeff"),Likes(likesUserImg: "steve"),
                         Likes(likesUserImg: "elon"),Likes(likesUserImg: "tim"),
                         Likes(likesUserImg: "jeff"),Likes(likesUserImg: "steve"),
                         Likes(likesUserImg: "elon"),Likes(likesUserImg: "tim"),
                         Likes(likesUserImg: "jeff"),Likes(likesUserImg: "steve")
    ]
        
    required init(collectionViewLayout: UICollectionViewFlowLayout) {
        super.init(collectionViewLayout: collectionViewLayout)
    }
    
    override func didLoad() {
        super.didLoad()
        delegate = self
        dataSource = self
        view.showsVerticalScrollIndicator = false
        view.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
    }
    
    @objc func didPullToRefresh() {
        self.reloadData()
        self.refreshControl.endRefreshing()
        eventDelegate?.didPullToRefresh()
    }
    
    func updateLikes(likes: [Likes]){
        self.likes += likes
    }

    func refreshLikes(likes: [Likes]) {
        self.refreshControl.endRefreshing()
        self.likes = likes
        self.reloadData()
    }
}

extension LikesViewerCollectionNode: ASCollectionDataSource {
    func collectionNode(_ collectionNode: ASCollectionNode, numberOfItemsInSection section: Int) -> Int {
        return likes.count
    }
    
    func collectionNode(_ collectionNode: ASCollectionNode, nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock {
        let like = likes[indexPath.item]
        let size = CGSize(width: (view.frame.width - 24)/3, height: (view.frame.width - 24)/3)
        return { LikesViewerCellNode(like: like, size: size) }
    }
}

extension LikesViewerCollectionNode : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
}


