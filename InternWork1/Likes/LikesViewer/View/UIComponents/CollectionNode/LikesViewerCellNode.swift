//
//  LikesViewerCellNode.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import AsyncDisplayKit

class LikesViewerCellNode: ASCellNode {
    // MARK: Constants
    
    // MARK: View components
    
    private let imageNode = ASImageNode()
    private var imageNodeSize = CGSize()
    
    // MARK: Lifecycle
    
    required init(like: Likes, size: CGSize){
        super.init()
        imageNode.image = UIImage(named: like.likesUserImg)
        imageNodeSize = size
        automaticallyManagesSubnodes = true
    }
    
    override func didLoad() {
        super.didLoad()
        setupView()
    }
    
    // MARK: View Value Assignments
    
    private func setupView() {
        imageNode.cornerRadius = 6.0
        imageNode.isOpaque = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        imageNode.style.preferredSize = imageNodeSize        
        return ASWrapperLayoutSpec(layoutElement: imageNode)
    }
    
}

