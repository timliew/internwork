//
//  LikesViewerPresenter.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  COPYRIGHT © 2018-PRESENT Tim Liew ALL RIGHTS RESERVED.
//

import CocoaLumberjack
import AsyncDisplayKit

final class LikesViewerPresenter: LikesViewerPresentation {

  // MARK: - Properties

  weak var view: LikesViewerView?
  var router: LikesViewerWireframe!
  var interactor: LikesViewerUseCase!

  // MARK: - Helpers

  // TODO: implement presentation methods
    func refreshLikes(like: Likes){
        interactor.refreshLikes(like: like)
    }
    
    func didPullToRefresh() {
        interactor.refreshLikes(like: Likes(likesUserImg: ""))
    }
}

extension LikesViewerPresenter: LikesViewerInteractorOutput {
    // TODO: implement interactor output methods

    func didFetchLikes(likes: [Likes]) {
        view?.event = .didFetchLikes(likes: likes)
    }
    
    func didRefreshLikes(likes: [Likes]) {
        view?.event = .didRefreshLikes(likes: likes)
    }
}
