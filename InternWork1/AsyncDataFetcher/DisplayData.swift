//
//  DisplayData.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/3/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import UIKit

/// A type to represent how model data should be displayed.
class DisplayData: NSObject {
    
    var image: String = "user"
    
    // Add additional properties for your own configuration here.
}
