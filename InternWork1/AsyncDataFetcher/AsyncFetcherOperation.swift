//
//  AsyncFetcherOperation.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/3/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import Foundation

class AsyncFetcherOperation: Operation {
    let identifier: String
    
    private(set) var fetchedData: DisplayData?
    
    init(identifier: String) {
        self.identifier = identifier
    }
    
    override func main() {
        Thread.sleep(until: Date().addingTimeInterval(1))
        guard !isCancelled else { return }
        
        fetchedData = DisplayData()
    }
}
