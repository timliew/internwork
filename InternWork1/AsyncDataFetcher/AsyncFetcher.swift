//
//  AsyncFetcher.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/3/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import Foundation

class AsyncFetcher {
    private let serialAccessQueue = OperationQueue()
    private let fetchQueue = OperationQueue()
    private var completionHandlers = [String: [(DisplayData?) -> Void]]()
    private var cache = NSCache<NSString, DisplayData>()
    
    init() {
        serialAccessQueue.maxConcurrentOperationCount = 1
    }
    
    func fetchAsync(_ identifier: String, completion: ((DisplayData?) -> Void)? = nil) {
        // Use the serial queue while we access the fetch queue and completion handlers.
        serialAccessQueue.addOperation {
            // If a completion block has been provided, store it.
            if let completion = completion {
                let handlers = self.completionHandlers[identifier, default: []]
                self.completionHandlers[identifier] = handlers + [completion]
            }
            
            self.fetchData(for: identifier)
        }
    }
    
    func fetchedData(for identifier: String) -> DisplayData? {
        return cache.object(forKey: identifier as NSString)
    }
    
    func cancelFetch(_ identifier: String) {
        serialAccessQueue.addOperation {
            self.fetchQueue.isSuspended = true
            defer {
                self.fetchQueue.isSuspended = false
            }
            
            self.operation(for: identifier)?.cancel()
            self.completionHandlers[identifier] = nil
        }
    }
    
    private func fetchData(for identifier: String) {
        // If a request has already been made for the object, do nothing more.
        guard operation(for: identifier) == nil else { return }
        
        if let data = fetchedData(for: identifier) {
            // The object has already been cached; call the completion handler with that object.
            invokeCompletionHandlers(for: identifier, with: data)
        } else {
            // Enqueue a request for the object.
            let operation = AsyncFetcherOperation(identifier: identifier)
            
            // Set the operation's completion block to cache the fetched object and call the associated completion blocks.
            operation.completionBlock = { [weak operation] in
                guard let fetchedData = operation?.fetchedData else { return }
                self.cache.setObject(fetchedData, forKey: identifier as NSString)
                
                self.serialAccessQueue.addOperation {
                    self.invokeCompletionHandlers(for: identifier, with: fetchedData)
                }
            }
            
            fetchQueue.addOperation(operation)
        }
    }
    
    private func operation(for identifier: String) -> AsyncFetcherOperation? {
        for case let fetchOperation as AsyncFetcherOperation in fetchQueue.operations
            where !fetchOperation.isCancelled && fetchOperation.identifier == identifier {
                return fetchOperation
        }
        
        return nil
    }
    
    private func invokeCompletionHandlers(for identifier: String, with fetchedData: DisplayData) {
        let completionHandlers = self.completionHandlers[identifier, default: []]
        self.completionHandlers[identifier] = nil
        
        for completionHandler in completionHandlers {
            completionHandler(fetchedData)
        }
    }
    
}
