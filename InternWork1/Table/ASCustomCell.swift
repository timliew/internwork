//
//  ASCustomCell.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/5/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit

class ASCustomCell: ASTextCellNode {
    
    required init(textString: String, indexSection: Int) {
        let insets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        var attributes = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18),             NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        
        if indexSection == 5 {
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .center
            attributes[NSAttributedStringKey.foregroundColor] = UIColor.red
            attributes[NSAttributedStringKey.paragraphStyle] = paragraph
        }
        
        super.init(attributes: attributes, insets: insets)
        
        text = textString
        accessoryType = indexSection != 5 ? .disclosureIndicator : .none
        backgroundColor = .white
        selectionStyle = .default
    }
    
}
