//
//  AsyncTableViewController.swift
//  InternWork1
//
//  Created by Timothy Liew on 4/4/18.
//  Copyright © 2018 Tim Liew. All rights reserved.
//

import UIKit
import AsyncDisplayKit

class AsyncTableViewController: ASViewController<ASDisplayNode> {
    
    // MARK: Variables
    private let header = ["PROFILE", "VERSION", "FEEDBACK", "ABOUT", "TERMS AND PRIVACY", ""]
    private var items = [0: ["Bookmark","Posts you have liked"], 1:["1.2.3"],
                 2:["Feedback"], 3:["Open Source"], 4:["Terms","Privacy Policy"], 5:["Log Out"]]
    private var itemContent = ""
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Settings"
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18)]
    }
    
    // MARK: Initializing Table Node
    var tableNode: ASTableNode {
        return node as! ASTableNode
    }

    init() {
        super.init(node: ASTableNode(style: .grouped))
        tableNode.delegate = self
        tableNode.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Table Node Configurations
extension AsyncTableViewController: ASTableDataSource, ASTableDelegate {
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return header.count
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return (items[section]?.count)!
    }

    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        itemContent = (items[indexPath.section]?.remove(at: 0))!
        let node = ASCustomCell(textString: itemContent, indexSection: indexPath.section)
        return { node }
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 1 {
            let likesVC = LikesViewerViewController()
            self.navigationController?.pushViewController(likesVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return header[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }

}
